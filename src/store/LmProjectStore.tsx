import {observable, action} from 'mobx';
import {deepCopyObject, smplRateValue, langCdDisplay} from '../utils/CommonUtils';
import {API, callApi} from '../service/ApiService';
import _ from "lodash";
import WavDecoder from 'wav-decoder';
import chardet from 'chardet';

interface TestFile {
  files: Array<File>;
  fileName: string;
  smplRate: number;
  duration: number;
  encoding: string;
}

export default class LmProjectStore {
  private root: any;
  constructor(root) {
    this.root = root;
  }
  // check step data is changed
  @observable step1Changed : boolean = false;
  getStep1Changed = () => { return this.step1Changed; }
  setStep1Changed = (step1Changed) => { this.step1Changed = step1Changed; }

  @observable step2Changed : boolean = false;
  getStep2Changed = () => { return this.step2Changed; }
  setStep2Changed = (step2Changed) => { this.step2Changed = step2Changed; }

  // projectDto
  @observable id: number = -1;    // 프로젝트 ID
  @observable projectName: string = 'T';  // 프로젝트 이름
  @observable projectDescription: string = 'D'; // 프로젝트 설명
  @observable projectType: string = 'T';  // 프로젝트 타입: STT, TTS, ...
  @observable regDt: string = ''; // 등록일자
  @observable modDt: string = ''; // 수정일자
  @observable modelName: string = ''; // 모델명

  @action setProjectInfo(prj: any){
    this.id = prj.id;
    this.projectDescription = prj.projectDescription;
    this.projectName = prj.projectName;
    this.projectType = prj.projectType;
  }

  // projectDto.stt
  @observable completedStep: number = 1;  // 완료 스텝
  @observable language: string = '';  // 언어
  @action getLanguage = () => {
    return this.language;
  }
  @action setLanguage = (language) => {
    console.log('LmProjectStore setLanguage:' + language);
    this.language = language;
    this.step1Changed = true;
  };

  @observable sampleRate: string = '';  // 샘플링레이트
  @action getSampleRate = () => {
    return this.sampleRate;
  }
  @action setSampleRate = (sampleRate) => {
    console.log('SttProjectStore setSampleRate:' + sampleRate);
    this.sampleRate = sampleRate;
    this.step1Changed = true;
  };

  @observable eos: string = ''; // EOS 사용여부 Y/N
  @action getEos = () => {
    return this.eos;
  }

  @action setEos = (eos) => {
    console.log('SttProjectStore setEos:' + eos);
    this.eos = eos;
    this.step1Changed = true;
  };

  @observable learningDataList: any = [];  // 학습에 사용할 데이터
  @action getLearningDataList = () => {
    return this.learningDataList;
  }

  @observable testingDataList: Array<TestFile> = new Array<TestFile>();  // 테스트에 사용할 데이터
  @action setTestingDataList = (testingDataList) => {
    this.testingDataList = this.testingDataList.concat(testingDataList);
  }
  @action clearTestingDataList = () => {
    this.testingDataList = [];
  }
  @action getTestingDataList = () => {
    return this.testingDataList;
  }

  @observable epochSize: number = 1;  // epoch 사이즈
  @action getEpochSize = () => {
    return this.epochSize;
  }
  @action setEpochSize = (epochSize) => {
    this.epochSize = epochSize;
    this.step2Changed = true;
  }

  @observable batchSize: number = 1;  // batch 사이즈
  @action getBatchSize = () => {
    return this.batchSize;
  }
  @action setBatchSize = (batchSize) => {
    this.batchSize = batchSize;
    this.step2Changed = true;
  }

  @observable rate: number = 0.1; // rate
  @action getRate(){ return this.rate; }
  @action setRate(rate){
    this.rate = rate;
    this.step2Changed = true;
  }

  @observable sampleLength: number = 600; // sample length
  @action getSampleLength(){ return this.sampleLength; }
  @action setSampleLength(sampleLength){
    this.sampleLength = sampleLength;
    this.step2Changed = true;
  }

  @observable minNoiseSample: number = 0; //
  @action getMinNoiseSample(){ return this.minNoiseSample; }
  @action setMinNoiseSample(minNoiseSample){
    this.minNoiseSample = minNoiseSample;
    this.step2Changed = true;
  }

  @observable maxNoiseSample: number = 0;
  @action getMaxNoiseSample(){ return this.maxNoiseSample; }
  @action setMaxNoiseSample(maxNoiseSample){
    this.maxNoiseSample = maxNoiseSample;
    this.step2Changed = true;
  }

  @observable minSnr: number = 0;
  @action getMinSnr(){ return this.minSnr; }
  @action setMinSnr(minSnr){
    this.minSnr = minSnr;
    this.step2Changed = true;
  }

  @observable maxSnr: number = 0;
  @action getMaxSnr(){ return this.maxSnr; }
  @action setMaxSnr(maxSnr){
    this.maxSnr = maxSnr;
    this.step2Changed = true;
  }

  @observable checkSave: number = 0;
  @action getCheckSave(){ return this.checkSave; }
  @action setCheckSave(checkSave){
    this.checkSave = checkSave;
    this.step2Changed = true;
  }

  @observable noiseDataList: any = [];

  @observable preTrainedModel;
  @action setPreTrainedModel = (modelId, isCommon) => {
    let list;
    if (isCommon) {
      list = this.root.legacyDataStore.preTrainedModelCommonList;
    }
    else {
      list = this.root.legacyDataStore.preTrainedModelPrivateList;
    }
    let model = list.find(obj => (obj.modelId === modelId));
    this.preTrainedModel = model;
  }

  @observable currentStep: number = 0;

  @observable testState: boolean = false; // 테스트 상태
  @action setTestState(state){
    this.testState = state;
  }
  @action getTestState(){ return this.testState; }

  setData = (data) => {
    this.id = data.id;
    this.projectName = data.projectName;
    this.projectDescription = data.projectDescription;
    this.projectType = data.projectType;
    this.modelName = data.modelName;
    this.regDt = data.regDt;
    this.modDt = data.modDt;

    this.completedStep = data.completedStep;
    this.currentStep = this.completedStep;

    this.language = data.language ? data.language : this.root.legacyDataStore.getLang()[0].code;
    this.eos = data.eos ? data.eos : 'Y';
    data.learningDataList.forEach(v => {
      if(!v.atchFilePairName){
        v.atchFilePairName = v.atchFileDisp;
      }
    });
    this.learningDataList = data.learningDataList;
    this.checkSave = data.checkSave;

    this.setStep(this.currentStep);
  }

  @action setStep = (no) => {
    // console.log('SttProjectStore setStep:' + no);
    this.currentStep = no;
    // TODO: learning data save
    if(this.completedStep < no) {
      this.completedStep = no;
    }
    switch (no) {
      case 0: this.onStep1Active(); break;
      case 1: this.onStep2Active(); break;
      case 2: this.onStep3Active(); break;
      case 3: this.onStep4Active(); break;
    }
  }

  @action getStep = () => {
    return this.currentStep;
  }

  @action setCompletedStep = (no) => {
    this.completedStep = no;
  }

  @action getCompletedStep = () => {
    return this.completedStep;
  }

  // db의 completedStep update 및 store 데이터 update
  @action updateStep = async (id, stepId) => {
      const _api = deepCopyObject(API.PROJECT_LM_UPDATE_STEP);
      _api.url = _api.url.replace('_ID_',id);
      _api.url = _api.url.replace('_STEP_', stepId);

      await callApi(_api).then((res)=>{
        if(res.status === 200){
          // this.currentStep = stepId;
          this.completedStep = stepId;
          this.setStep(stepId);
        }else{
          this.root.notiStore.error('step update fail');
          console.log('%c HTTP status : ' + res.status + ' : step update fail\n', res.data);
        }
      }).catch((res)=>{
        this.root.notiStore.error('step update fail');
        console.log('%c HTTP status : ' + res.status + ' : step update fail\n', res.data);
      })
  }

  fetchData = async (id) => {
    const _api = deepCopyObject(API.PROJECT_LM_DETAIL);
    _api.url = _api.url.replace('_ID_', id);
    const res = await callApi(_api);
    console.log('get lm project data success:');
    console.log(res.data);
    this.setData(res.data);
    this.root.sttProjectStore.setData(res.data);
  }

// ----------------------------- 데이터 선택 (step 01) ----------------------------- //
  @action
  saveLearningData = async (stepId, setModelRegPopPopShow, modelRegPopShow) => {
    const _api = deepCopyObject(API.PROJECT_LM_REG_LEARNING_DATA);
    _api.url = _api.url.replace('_ID_', this.id);

    let param = {
      eos : this.eos,
      language : this.language,
      sampleRate : this.sampleRate,
      learningDataList : Object(this.learningDataList)
    }

    await callApi(_api, param).then((res)=>{
      if(res.status === 200){
        this.root.notiStore.infoGreen('학습 데이터 저장 성공');
        console.log('%c HTTP status : '+ res.status + ' : saveLearningData success');
        this.step1Changed = false;
        setModelRegPopPopShow(!modelRegPopShow);
        this.step2Changed = false;
      }else{
        this.root.notiStore.error('학습 데이터 저장 실패');
        console.log('%c HTTP status : ' + res.status + ' : saveLearningData fail\n', res.data);
      }
    }).catch((res)=>{
      this.root.notiStore.error('학습 데이터 저장 실패');
      console.log('%c HTTP status : ' + res.status + ' : saveLearningData fail\n', res.data);
    })


  }

  @action
  deleteData = async(id) => {
    const notiStore = this.root.notiStore;
    const _api = deepCopyObject(API.PROJECT_FILE_DELETE);
    _api.url = _api.url.replace('_ID_', id);
    await callApi(_api).then((res)=>{
      this.learningDataList = this.learningDataList.filter(v => v.atchFileId !== id);
      notiStore.info("파일 삭제가 완료되었습니다.");
    }).catch((res)=>{
      notiStore.error(`처리 중 오류가 발생했습니다. 사유 : ${res}`);
    })
  }

  @action
  uploadData = async (step, fileData) => {
    const files = fileData ? fileData : this.root.legacyDataStore.learningDataList;
    const notiStore = this.root.notiStore;
    if(files.length === 0){
      notiStore.error('업로드 할 파일이 없습니다.');
      return;
    }
    let valid: boolean = false;
    // 유효성 검증
    // @ts-ignore
    if (Array.from(files).map(v => v.name.split('.').pop()?.toLowerCase()).find(v => v !== 'txt')) {
      notiStore.error('파일의 확장자는 txt만 허용됩니다.');
      console.log(`uploadLearningData - file exp mismatch`);
    } else {
      valid = true;
    }

    // API CALL
    if(valid){
      if(step === 1){
        await this.uploadLearningData(files);
      }else if(step === 4){
        Array.from(files).forEach((v) => {
          const key = (v as File).name.split('.')[0];
          const reader = new FileReader();
          reader.readAsArrayBuffer(v as Blob);
          reader.onload = async (e) => {
            let testFile = {} as TestFile;
            if((v as File).type.indexOf('audio') > -1){
              let ctx = new AudioContext();
              const wavDecode = WavDecoder.decode(e.target?.result as ArrayBuffer);
              const audioDecode = ctx.decodeAudioData(e.target?.result as ArrayBuffer)
              await Promise.all([wavDecode, audioDecode]).then((audioData) => {
                if(audioData[0].sampleRate === smplRateValue(this.root.modelTestStore.currentTargetModel.smplRate)){
                  testFile.duration = Math.round((audioData[1].duration * 100) / 100);
                  testFile.smplRate = audioData[0].sampleRate;
                }else{
                  notiStore.error(`음성 파일의 샘플레이트가 프로젝트 설정의 샘플레이트와 일치하지 않습니다. 파일 샘플레이트 : ${audioData[0].sampleRate}`);
                  return;
                }
              });
            }else{
              const encoding = chardet.detect(Buffer.from(e.target?.result as ArrayBuffer))?.toLocaleLowerCase();
              if(encoding === 'utf-8'){
                const text = new TextDecoder().decode(e.target?.result as ArrayBuffer);
                const textCheck = this.getLanguage() === 'LN_KO' ? /^[ㄱ-ㅎ|가-힣]*$/.test(text) : /^[a-z|A-z]*$/gi.test(text);
                if(textCheck){
                  testFile.encoding = encoding;
                }else{
                  notiStore.error(`텍스트 파일의 문자열이 프로젝트 설정과 일치하지 않습니다. 프로젝트 설정 : ${langCdDisplay(this.getLanguage())}`);
                  return;
                }
              }else{
                notiStore.error(`텍스트 파일의 인코딩이 올바르지 않습니다. 인코딩 : ${encoding}`);
                return;
              }
            }
            if(!testFile.fileName) testFile.fileName = key;
            if(!testFile.files) testFile.files = new Array();
            testFile.files.push(v as File);
            let fileIndex = this.testingDataList.findIndex((v => v.fileName === key));
            if(fileIndex > -1){
              testFile.files = testFile.files.concat(this.testingDataList[fileIndex].files);
              testFile = _.merge({}, this.testingDataList[fileIndex], testFile);
              this.testingDataList.splice(fileIndex, 1, testFile);
            }else{
              this.testingDataList.push(testFile);
            }
          };
        });
      }
    }

  }

  @action
  uploadTestData = async (step, fileData) => {
    const files = fileData ? fileData : this.root.legacyDataStore.learningDataList;
    const notiStore = this.root.notiStore;
    if(files.length === 0){
      notiStore.error('업로드 할 파일이 없습니다.');
      return;
    }
    let valid: boolean = false;
    // 유효성 검증
    if (files && files.length % 2 !== 0) {
      notiStore.error('파일의 쌍이 올바르지 않습니다.');
      console.log(`uploadLearningData - file pair mismatch`);
      // @ts-ignore
    } else if (Array.from(files).map(v => v.name.split('.').pop()?.toLowerCase()).find(v => v !== 'wav' && v !== 'txt')) {
      notiStore.error('파일의 확장자는 wav, txt만 허용됩니다.');
      console.log(`uploadLearningData - file exp mismatch`);
    } else {
      // @ts-ignore
      const fileMap: Map<String | number, Array<File>> = _.groupBy(Array.from(files), (v) => v.name.split('.').shift());
      const badFiles: Array<File> = Object.values(fileMap).filter(v => v.length > 2 || v.length < 2);
      if(badFiles.length > 0) {
        notiStore.error(`(${_.flatten(badFiles).map(v => v.name).join(', ')}) 파일의 업로드 쌍이 잘못되었습니다.`);
        console.log(`uploadLearningData - file pair mismatch : ${_.flatten(badFiles).map(v => v.name).join(', ')}`);
      } else {
        valid = true;
      }
    }

    // API CALL
    if(valid){
      const readFile = (idx) => {
        if( idx >= files.length ) return;
        const key = (files[idx] as File).name.split('.')[0];
        const reader = new FileReader();
        const bind = (r) => {
          if(r){
            readFile(idx+1);
          }else{
            this.clearTestingDataList();
          }
        }
        reader.readAsArrayBuffer(files[idx] as Blob);
        reader.onload = async (e) => {
          let result = true;
          let testFile = {} as TestFile;
          if((files[idx] as File).type.indexOf('audio') > -1){
            let ctx = new AudioContext();
            const wavDecode = WavDecoder.decode(e.target?.result as ArrayBuffer);
            const audioDecode = ctx.decodeAudioData(e.target?.result as ArrayBuffer)
            result = await Promise.all([wavDecode, audioDecode]).then((audioData) => {
              if(audioData[0].sampleRate === smplRateValue(this.root.modelTestStore.currentTargetModel.sampleRate)){
                testFile.duration = Math.round((audioData[1].duration * 100) / 100);
                testFile.smplRate = audioData[0].sampleRate;
                return true;
              }else{
                notiStore.error(`음성 파일의 샘플레이트가 프로젝트 설정의 샘플레이트와 일치하지 않습니다. 파일 샘플레이트 : ${audioData[0].sampleRate}`);
                return false;
              }
            });
          }else{
            const encoding = chardet.detect(Buffer.from(e.target?.result as ArrayBuffer))?.toLocaleLowerCase();
            if(encoding === 'utf-8'){
              const text = new TextDecoder().decode(e.target?.result as ArrayBuffer);
              const textCheck = this.getLanguage() === 'LN_KO' ? /^[ㄱ-ㅎ|가-힣]*$/.test(text) : /^[a-z|A-z]*$/gi.test(text);
              if(textCheck){
                testFile.encoding = encoding;
              }else{
                notiStore.error(`텍스트 파일의 문자열이 프로젝트 설정과 일치하지 않습니다. 프로젝트 설정 : ${langCdDisplay(this.getLanguage())}`);
                result = false;
              }
            }else{
              notiStore.error(`텍스트 파일의 인코딩이 올바르지 않습니다. 인코딩 : ${encoding}`);
              result = false;
            }
          }
          if(result){
            if(!testFile.fileName) testFile.fileName = key;
            if(!testFile.files) testFile.files = new Array();
            testFile.files.push(files[idx] as File);
            let fileIndex = this.testingDataList.findIndex((v => v.fileName === key));
            if(fileIndex > -1){
              testFile.files = testFile.files.concat(this.testingDataList[fileIndex].files);
              testFile = _.merge({}, this.testingDataList[fileIndex], testFile);
              this.testingDataList.splice(fileIndex, 1, testFile);
            }else{
              this.testingDataList.push(testFile);
            }
          }
          bind(result);
        };
      }
      readFile(0);
    }
  }

  @action
  uploadLearningData = async (files: any) => {
    const notiStore = this.root.notiStore;
    const _api = deepCopyObject(API.PROJECT_LM_REG_LEARNING_DATA_FILE);
    _api.url = _api.url.replace('_ID_', this.id);
    const formData: FormData = new FormData();
    // @ts-ignore
    Array.from(files).forEach(v =>formData.append('files', v));
    formData.append('lang', this.language);
    await callApi(_api, formData).then((res)=>{
      if(res.data.success || res.data.success == false){
        notiStore.error(`처리 중 오류가 발생했습니다. 사유 : ${res.data.message}`);
      }else{
        this.learningDataList = this.learningDataList.concat(res.data);
        console.log(this.learningDataList);
        this.root.legacyDataStore.learningDataList = [];
      }
    }).catch((res)=>{
      notiStore.error(`처리 중 오류가 발생했습니다. 사유 : ${res.data.message}`);
    })
  }

  @action
  putLearningData(item: any) {
    if(typeof this.learningDataList === 'undefined') {
      this.learningDataList = [];
    }
    this.learningDataList.push(item);
    this.step1Changed = true;
  }

  @action
  takeLearningData(item: any) {
    if(typeof this.learningDataList === 'undefined') {
      this.learningDataList = [];
      return;
    }
    this.learningDataList.remove(item);
    this.step1Changed = true;
  }


  // ----------------------------- 학습 설정 (step 02) ----------------------------- //

  /*@action
  saveTrainSetting = async (setModelRegPopPopShow, modelRegPopShow) =>{
    const _api = deepCopyObject(API.PROJECT_LM_REG_TRAINING_DATA);
    _api.url = _api.url.replace('_ID_', this.id);
    let param = {
      batchSize : this.batchSize,
      epochSize : this.epochSize,
      rate : this.rate,
      sampleLength : this.sampleLength,
      maxNoiseSample : this.maxNoiseSample,
      minNoiseSample : this.minNoiseSample,
      maxSnr : this.maxSnr,
      minSnr : this.minSnr,
      checkSave : this.checkSave,
      noiseDataList : Object(this.noiseDataList),
      preTrainedModel : Object(this.preTrainedModel)
    }

    await callApi(_api, param).then((res)=>{
      if(res.status === 200){
        this.root.notiStore.infoGreen('학습 설정 저장 성공');
        console.log('%c HTTP status : '+ res.status + ' : saveTrainSetting success');
        setModelRegPopPopShow(!modelRegPopShow);
        this.step2Changed = false;
      }else{
        this.root.notiStore.error('학습 설정 저장 실패');
        console.log('%c HTTP status : ' + res.status + ' : saveTrainSetting fail\n', res.data);
      }
    }).catch((res)=>{
      this.root.notiStore.error('학습 설정 저장 실패');
      console.log('%c HTTP status : ' + res.status + ' : saveTrainSetting fail\n', res.data);
    })
  }*/

  // ----------------------------- 학습 결과확인 & 테스트 & 다운로드 (step 04) ----------------------------- //

  @action
  startModelTest = async (files: any) => {
    const notiStore = this.root.notiStore;
    const _api = deepCopyObject(API.MODEL_TEST_START);
    const formData: FormData = new FormData();
    // @ts-ignore
    Array.from(files).forEach(v =>formData.append('files', v));
    formData.append('projectSeq', this.root.modelTestStore.currentTargetModel.projectSeq || 0);
    formData.append('snapshotSeq', this.root.modelTestStore.currentTargetModel.snapshotSeq || 0);
    formData.append('modelFileName', this.root.modelTestStore.currentTargetModel.modelFileName);
    formData.append('targetProjectSeq', this.root.modelTestStore.currentModel.projectSeq);
    formData.append('targetSnapshotSeq', this.root.modelTestStore.currentModel.snapshotSeq);
    formData.append('targetModelFileName', this.root.modelTestStore.currentModel.modelFileName);
    formData.append('lang', this.language);
    formData.append('smplRate', String(smplRateValue(this.root.modelTestStore.currentTargetModel.sampleRate)));
    await callApi(_api, formData).then((res)=>{
      this.setTestState(true);
    }).catch((res)=>{
      notiStore.error(`처리 중 오류가 발생했습니다. 사유 : ${res.data.message}`);
      this.setTestState(false);
    })
  }

  @action
  downloadModel = async () => {
    const notiStore = this.root.notiStore;
    if(!this.root.modelTestStore.currentModel){
      notiStore.error('다운로드 할 모델을 선택해주세요.');
    }else{
      const formData: FormData = new FormData();
      formData.append('projectSeq', this.root.modelTestStore.currentModel.projectSeq);
      formData.append('snapshotSeq', this.root.modelTestStore.currentModel.snapshotSeq);
      formData.append('modelFileName', this.root.modelTestStore.currentModel.modelFileName);
      const _api = deepCopyObject(API.MODEL_DOWNLOAD);
      await callApi(_api, formData).then((res)=>{
        console.log(res);
        const url = window.URL.createObjectURL(new Blob([res.data]));
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute(
            'download',
            this.root.modelTestStore.currentModel.modelFileName,
        );
        document.body.appendChild(link);
        link.click();
        // @ts-ignore
        link.parentNode.removeChild(link);
        notiStore.info(`파일 다운로드 완료`);
      }).catch((res)=>{
        notiStore.error(`처리 중 오류가 발생했습니다. 사유 : ${res.data.message}`);
      })
    }
  }

  @action
  putNoiseData(item: any){
    if(typeof this.noiseDataList === 'undefined'){
      this.noiseDataList = [];
    }
    this.noiseDataList.push(item);
    this.step2Changed = true;
  }

  @action
  takeNoiseData(item: any){
    if(typeof this.noiseDataList === 'undefined'){
      this.noiseDataList = [];
      return;
    }
    this.noiseDataList.remove(item);
    this.step2Changed = true;
  }

  onStep1Active()  {
    console.log('on Step1 Active');
    this.root.legacyDataStore.fetchLearningData(false);
    this.root.trainingStore.clearData();
  }
  onStep2Active()  {
    console.log('on Step2 Active');
    this.root.legacyDataStore.fetchPreTrainedModel();
    void this.root.legacyDataStore.fetchNoiseData();
    this.root.trainingStore.clearData();
  }
  onStep3Active()  {
    console.log('on Step3 Active');
    void this.root.trainingStore.fetchTrainingData(this.id);
  }
  onStep4Active()  {
    console.log('on Step4 Active');
    void this.root.trainingStore.fetchTrainingData(this.id);
    this.root.legacyDataStore.fetchPreTrainedModel();
    void this.root.legacyDataStore.fetchNoiseData();
    void this.root.modelTestStore.fetchModelList(this.root.lmProjectStore.id, this.root.lmProjectStore.projectType);
    this.root.modelTestStore.fetchTargetModelList(this.root.lmProjectStore.projectType);
  }

}
