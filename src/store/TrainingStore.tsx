import {observable, action} from 'mobx';
import {deepCopyObject} from '../utils/CommonUtils';
import {API, callApi} from '../service/ApiService';

export default class TrainingStore {
    private root: any;

    constructor(root) {
        this.root = root;
    }

    @observable snapshotId : number = 0;
    @action getSnapshotId(){ return this.snapshotId; }
    @action setSnapshotId(snapshotId){ this.snapshotId = snapshotId; }


    @observable status : string = '';
    @action getStatus(){ return this.status; }
    @action setStatus(status){ this.status = status; }

    @observable order : number = 0;
    @action getOrder(){ return this.order; }
    @action setOrder(order){ this.order = order; }

    @observable modelId : string = '';
    @action getModelId(){ return this.modelId; }
    @action setModelId(modelId){ this.modelId = modelId;}

    @observable dataPoints : any = [];
    @action getDataPoints(){ return this.dataPoints; }
    @action setDataPoints(dataPoints){ this.dataPoints = dataPoints; }

    @observable timePerSession: string = ''; // time
    @action getTimePerSession(){ return this.timePerSession; }
    @action setTimePerSession(timePerSession){
        this.timePerSession = timePerSession;
    }


    @action setData = (data) => {
        this.setSnapshotId(data.snapshotId);
        this.setStatus(data.status);
        this.setOrder(data.order);
        this.setModelId(data.modelId);
        this.setDataPoints(data.dataPoints);
        this.setTimePerSession(data.timePerSession);
    }

    @action clearData = () => {
        // this.setSnapshotId(0);
        this.setStatus('');
        // this.setOrder(0);
        // this.setModelId('');
        // this.setDataPoints([]);
    }

    @action
    fetchTrainingData = async (projectId) => {
        const _api = deepCopyObject(API.TRAINING_MONITORING);
        _api.url = _api.url.replace('_ID_', projectId);

        await callApi(_api).then((res)=>{
            if(res.status === 200){
                console.dir(res.data);
                if(res.data.status === 'e' && this.root.sttProjectStore.completedStep === 3){
                    this.root.sttProjectStore.updateStep(projectId, 2);
                }
                if(res.data.status === 'e' && this.root.lmProjectStore.completedStep === 3){
                    this.root.lmProjectStore.updateStep(projectId, 2);
                }
                this.setData(res.data);
                this.root.notiStore.infoGreen('학습 상태 가져오기 성공!!');
            }else{
                this.root.notiStore.error('학습 상태 가져오기 실패');
                console.log('%c HTTP status : ' + res.status + ' : fetchTrainingData fail\n', res.data);
            }
        }).catch((res)=>{
            this.root.notiStore.error('학습 상태 가져오기 실패');
            console.log('%c HTTP status : ' + res.status + ' : fetchTrainingData fail\n', res.data);
        })
    }

    @action
    fetchTrainingDataUpdate = async (projectId) => {
        console.log('%c TrainingStore/fetchTrainingDataUpdate()', 'color:blue');
        const _api = deepCopyObject(API.TRAINING_MONITORING);
        _api.url = _api.url.replace('_ID_', projectId);
        let delayFunc;
        let timerId = setTimeout(delayFunc = async () => {
            await callApi(_api).then((res) => {
                if (res.data === undefined || res.data === '') {
                    console.log('HTTP status : ' + res.status + ' : fetchTrainingDataUpdate fail, empty result.\n');
                    return false;
                }
                console.dir(res.data.status);
                let now = new Date();
                switch (res.data.status) {
                    case 'c':
                        console.log('%cSnapshot status : c', 'color:green');
                        if (this.root.sttProjectStore.completedStep === 2) {
                            this.root.sttProjectStore.updateStep(projectId, 3);
                        }
                        if (this.root.lmProjectStore.completedStep === 2) {
                            this.root.lmProjectStore.updateStep(projectId, 3);
                        }
                        break;
                    case 'e' :
                        console.log('%cSnapshot status : e', 'color:red');
                        if(this.root.sttProjectStore.completedStep === 3){
                            this.root.sttProjectStore.updateStep(projectId, 2);
                        }
                        if(this.root.lmProjectStore.completedStep === 3){
                            this.root.lmProjectStore.updateStep(projectId, 2);
                        }
                        break;
                    default :
                        break;
                }

                this.setData(res.data);

            }).catch((res) => {
                console.log('HTTP status : ' + res.status + ' : fetchTrainingDataUpdate fail\n', res.data.status);
            })
        }, 60000);
    }



    @action
    trainingCancel = async () => {

        console.log('trainingCancel() :: ', this.snapshotId, this.modelId);

        let param = {id: this.snapshotId, modelId: this.modelId};
        const _api = deepCopyObject(API.TRAINING_CANCEL);

        await callApi(_api, param).then((res) => {
            if (res.status === 200) {
                console.dir(res.data);
                this.root.sttProjectStore.completedStep = this.root.sttProjectStore.getStep() - 1;
                this.root.sttProjectStore.setStep(this.root.sttProjectStore.getStep() - 1);
                this.root.lmProjectStore.completedStep = this.root.lmProjectStore.getStep() - 1;
                this.root.lmProjectStore.setStep(this.root.lmProjectStore.getStep() - 1);
                this.root.notiStore.infoGreen('학습 취소 성공');
            } else {
                this.root.notiStore.error('학습 취소 실패');
                console.log('%c HTTP status : ' + res.status + ' : cancel training fail\n', res.data);
            }
        }).catch((res) => {
            this.root.notiStore.error('학습 취소 실패');
            console.log('%c HTTP status : ' + res.status + ' : cancel training fail\n', res.data);
        })

    }



}
