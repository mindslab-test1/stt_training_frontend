import LayoutStore from './LayoutStore';
import UserStore from './UserStore';
import SttProjectStore from './SttProjectStore';
import LmProjectStore from './LmProjectStore';
import LegacyDataStore from './LegacyDataStore';
import NotificationStore from './NotificationStore';
import TrainingStore from './TrainingStore';
import ModelTestStore from './ModelTestStore';

import {createContext} from 'react';


class RootStore {
  layoutStore: LayoutStore;
  userStore: UserStore;
  sttProjectStore: SttProjectStore;
  lmProjectStore: LmProjectStore;
  legacyDataStore: LegacyDataStore;
  notiStore: NotificationStore;
  trainingStore: TrainingStore;
  modelTestStore: ModelTestStore;

  constructor() {
    this.layoutStore = new LayoutStore(this);
    this.userStore = new UserStore(this);
    this.sttProjectStore = new SttProjectStore(this);
    this.lmProjectStore = new LmProjectStore(this);
    this.legacyDataStore = new LegacyDataStore(this);
    this.notiStore = new NotificationStore(this);
    this.trainingStore = new TrainingStore(this);
    this.modelTestStore = new ModelTestStore(this);
  }
}

export default createContext(new RootStore());
