import {action, computed, observable, toJS} from 'mobx';
import {deepCopyObject} from '../utils/CommonUtils';
import {API, callApi, callMockApi} from '../service/ApiService';
import SttProjectStore from './SttProjectStore';

export default class LegacyDataStore {
  private root: any;
  private sttProjectStore: SttProjectStore;
  constructor(root) {
    this.root = root;
    this.sttProjectStore = root.sttProjectStore;
  }

  @observable lang: any = [];
  @action setLang = (lang) => {
    this.lang = lang;
  };
  @action getLang = () => {return this.lang;};

  @observable sampleRate: any = [];
  @action setSampleRate = (sampleRate) => {
    this.sampleRate = sampleRate;
  };
  @action getSampleRate = () => {return this.sampleRate;};

  @observable learningDataList: any[] = [];
  @action setLearnData = (files) => {
    if(this.root.sttProjectStore.learningDataList.length === 0) {
      this.learningDataList = files;
    }else{
      this.learningDataList = Array.from(files).filter(
          // @ts-ignore
          it => this.root.sttProjectStore.learningDataList.every((x) => it.name.indexOf(x.atchFilePairName) === -1)
      );
    }
  };
  @action getLearnData = () => {return this.learningDataList;};


  @observable preTrainedModelCommonList: any = undefined;
  @observable preTrainedModelCommonListDisplay: any = undefined;

  @observable preTrainedModelPrivateList: any = undefined;
  @observable preTrainedModelPrivateListDisplay: any = undefined;

  @computed get getPreTrainedModelCommon() {
    console.log('getPreTrainedModelCommon');
    if (this.preTrainedModelCommonListDisplay === undefined) {
      void this.fetchPreTrainedModelCommon();
    }
    return this.preTrainedModelCommonListDisplay;
  }

  @computed get getPreTrainedModelPrivate() {
    console.log('getPreTrainedModelPrivate');
    if (this.preTrainedModelPrivateListDisplay === undefined) {
      void this.fetchPreTrainedModelPrivate();
    }
    return this.preTrainedModelPrivateListDisplay;
  }

  @computed get getAvailLearnData() {
    console.log('getAvailLearnData');
    if(typeof this.root.sttProjectStore.learningData === 'undefined') {
      this.root.sttProjectStore.learningData = [];
    }

    // TODO 해당부분 어떤로직인지 확인하고 수정필요. 아마도 학습데이터와 중복되지 않게 하려고 했던거 같음.
    const list = Array.from(this.learningDataList).filter(
      it => this.root.sttProjectStore.learningDataList.every((x) => it.name.indexOf(x.atchFilePairName) === -1)
    );

    return list;
  }

  @observable noiseData: any = [];
  @action setNoiseData = (noiseData) => {this.noiseData = noiseData;}
  @action getNoiseData = () => { return this.noiseData; }
  @computed get getAvailNoiseData() {
    console.log('getAvailNoiseData START');
    if(typeof this.root.sttProjectStore.noiseDataList === 'undefined') {
      this.root.sttProjectStore.noiseDataList = [];
    }

    const list = this.noiseData;

    console.log('getAvailNoiseData END: ',list);
    return list;
  }
  @observable modelFileList : any = [];
  @action setModelFileList = (modelFileList) => {this.modelFileList = modelFileList;}
  @action getModelFileList = () => { return this.modelFileList; }

  @action
  fetchLangData = async () => {
    const _api = deepCopyObject(API.CODE_GROUP);
    _api.url = _api.url.replace('_CODE-GROUP_', 'LANG');
    const res = await callApi(_api);
    console.log('get fetchLangData');
    console.log(res.data);
    this.setLang(res.data.codes);
  }

  @action
  fetchSampleRateData = async () => {
    const _api = deepCopyObject(API.CODE_GROUP);
    _api.url = _api.url.replace('_CODE-GROUP_', 'SAMPLE_RATE');
    const res = await callApi(_api);
    console.log('get fetchSampleRateData');
    console.log(res.data);
    this.setSampleRate(res.data.codes);
  }

  @action
  fetchLearningData = async (bInit: boolean) => {
    const params = {
      engnType: 'EN_STT',
      langCd: this.sttProjectStore.getLanguage(),
      smplRate: this.sttProjectStore.getSampleRate(),
      useEosYn: this.sttProjectStore.getEos(),
    }
    const _api = deepCopyObject(API.DMNG_LEARNING_DATA_LIST);
    const res = await callApi(_api, params);
    console.log('get fetchLearningData success:');
    console.log(res.data);
    this.learningDataList = res.data;
    if(bInit) {
      this.root.sttProjectStore.learningDataList = [];
    }
  }

  @action
  fetchNoiseData = async () => {
    const params = {
      engnType: 'EN_STT',
      langCd: this.sttProjectStore.getLanguage(),
      smplRate: this.sttProjectStore.getSampleRate(),
      useEosYn: this.sttProjectStore.getEos(),
    }
    const _api = deepCopyObject(API.PROJECT_STT_NOISE_DATA_LIST);
    const res = await callApi(_api, params);
    console.log('get fetchNoiseData success:');
    console.log(res.data);
    this.noiseData = res.data;
  }

  // private isTrainedModelInitialized = false;
  @action
  fetchPreTrainedModel() {
      void this.fetchPreTrainedModelCommon();
      void this.fetchPreTrainedModelPrivate();
  }

  @action
  fetchPreTrainedModelCommon = async () => {
    const params = {
      pre: true,
      engnType: 'EN_STT',
      langCd: this.sttProjectStore.getLanguage(),
      smplRate: this.sttProjectStore.getSampleRate(),
      useEosYn: this.sttProjectStore.getEos(),
    }
    const _api = deepCopyObject(API.PROJECT_MODEL_DATA_LIST);
    const res = await callApi(_api, params);
    console.log('get fetchPreTrainedModelCommon success:');
    if(res.data.length > 0) {
      this.preTrainedModelCommonList = res.data;
      this.preTrainedModelCommonListDisplay = res.data;
    }
  }

  @action
  fetchPreTrainedModelPrivate = async () => {
    const params = {
      pre: false,
      engnType: 'EN_STT',
      langCd: this.sttProjectStore.getLanguage(),
      smplRate: this.sttProjectStore.getSampleRate(),
      useEosYn: this.sttProjectStore.getEos(),
    }
    const _api = deepCopyObject(API.PROJECT_MODEL_DATA_LIST);
    const res = await callApi(_api, params);
    console.log('get fetchPreTrainedModelPrivate success:');
    if(res.data.length > 0) {
      this.preTrainedModelPrivateList = res.data;
      this.preTrainedModelPrivateListDisplay = res.data;
    }
  }

  @action
  fetchModelFileList = async (projectId) => {
    const _api = deepCopyObject(API.DMNG_MODELFILE_LIST);
    _api.url = _api.url.replace('_ID_', projectId);

    await callApi(_api).then((res)=>{
      if(res.status === 200){
        console.dir(res.data); // Array 나옴
        this.root.notiStore.infoGreen('학습 모델 파일 리스트 가져오기 성공');
        this.modelFileList = res.data;
        console.log('%c HTTP status : '+ res.status + ' : getModelFileList success');
      }else{
        this.root.notiStore.error('학습 모델 파일 리스트 가져오기 실패');
        console.log('%c HTTP status : ' + res.status + ' : getModelFileList fail\n', res.data);
      }
    }).catch((res)=>{
      this.root.notiStore.error('학습 모델 파일 리스트 가져오기 실패');
      console.log('%c HTTP status : ' + res.status + ' : getModelFileList fail\n', res.data);
    })

  }
}
