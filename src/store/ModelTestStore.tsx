import {action, computed, observable} from 'mobx';
import {deepCopyObject, guid} from '../utils/CommonUtils';
import {API, callApi, callTestApi} from '../service/ApiService';
import SttProjectStore from './SttProjectStore';
import LmProjectStore from "./LmProjectStore";

export default class ModelTestStore {
  private root: any;
  private sttProjectStore: SttProjectStore;
  private lmProjectStore: LmProjectStore;
  constructor(root) {
    this.root = root;
    this.sttProjectStore = root.sttProjectStore;
    this.lmProjectStore = root.lmProjectStore;
  }

  @observable currentModel;
  @observable currentTargetModel;
  @observable cmplCheck = false;
  @action setCmplCheck = (check) => {
    this.cmplCheck = check;
  }
  @action getCmplCheck = () => {
    return this.cmplCheck;
  }
  @action setCurrentModel = (modelName, id) => {
    // if (this.testModelList !== undefined && this.testModelList.length > 0) {
    //   let model = this.testModelList.find(obj => (obj.mdlId === id));
    //   if (model) {
    //     let modelFile = model.find(obj => (obj.mdlFileName === modelName && obj.mdlId === id));
    //     this.currentModel = modelFile;
    //   }
    // }

    //
    //   for (const model of this.testModelList) {
    //     for(const modelFile of model.modelFileInfoVoList) {
    //
    //     }
    const model = this.testModelList.find(obj => obj.modelId === id);
    this.currentModel = model;
  }
  @action setCurrentTargetModel = (modelName, id) => {
    // if (this.testModelList !== undefined && this.testModelList.length > 0) {
    //   let model = this.testModelList.find(obj => (obj.mdlId === id));
    //   if (model) {
    //     let modelFile = model.find(obj => (obj.mdlFileName === modelName && obj.mdlId === id));
    //     this.currentModel = modelFile;
    //   }
    // }

    //
    //   for (const model of this.testModelList) {
    //     for(const modelFile of model.modelFileInfoVoList) {
    //
    //     }

    const model = this.testTargetModelList.find(obj => obj.modelId === id);
    if(this.root.lmProjectStore.projectType === 'LM'){
      this.root.lmProjectStore.testingDataList = [];
    }
    this.currentTargetModel = model;
  }

  @observable testModelList: any = undefined;
  @action setTestModelList = (testModelList) => {this.testModelList = testModelList;}
  @observable testTargetModelList: any = undefined;
  @action setTestTargetModelList = (testTargetModelList) => {this.testTargetModelList = testTargetModelList;}
  // @action getTestModelList = () => {return this.testModelList;}

  @observable modelListDisplay: any = undefined;
  @observable targetModelListDisplay: any = undefined;

  @computed get getTestModelList() {
    console.log('getTestModelList');
    if (this.testModelList === undefined) {
      return [];
      // void this.fetchModelList(this.root.sttProjectStore.id);
    }

    return this.modelListDisplay;
  }

  @computed get getTargetModelList() {
    console.log('getTargetModelList');
    if (this.testTargetModelList === undefined) {
      return [];
      // void this.fetchModelList(this.root.sttProjectStore.id);
    }

    return this.testTargetModelList;
  }

  @observable stepNum : number = 1;
  @action getStepNum(){ return this.stepNum; }
  @action setStepNum(stepNum){ this.stepNum = stepNum; }

  @observable queueIndex : number = 0;
  @action getQueueIndex(){ return this.queueIndex; }
  @action setQueueIndex(queueIndex){ this.queueIndex = queueIndex;}

  @action
  fetchModelList = async (id: number, projectType: string) => {
    const _api = deepCopyObject(API.PROJECT_TEST_MODEL_DATA_LIST);
    _api.url = _api.url.replace('_ID_', id);
    let params;
    if(projectType === 'LM'){
      params = {
        engnType: 'LM',
        langCd: this.lmProjectStore.getLanguage(),
        useEosYn: this.lmProjectStore.getEos(),
      }
    }else{
      params = {
        engnType: 'EN_STT',
        langCd: this.sttProjectStore.getLanguage(),
        smplRate: this.sttProjectStore.getSampleRate(),
        useEosYn: this.sttProjectStore.getEos(),
      }
    }
    const res = await callApi(_api, params);
    console.log('get fetchTestModelList');
    if (res.data !== undefined && res.data.length > 0) {
      const lst = res.data.reduce((arr, v) => {
        if(projectType === 'LM'){
          arr.push([{modelPath: v.modelPath, projectSeq: v.projectSeq, snapshotSeq: v.snapshotSeq, modelId: v.modelId+'-'+v.modelFileName, modelFileName: v.modelFileName, modelName: v.modelName+'-'+v.modelFileName}])
        }else{
          arr.push([{modelPath: v.modelPath, projectSeq: v.projectSeq, snapshotSeq: v.snapshotSeq, modelId: v.modelId+'-'+v.modelFileName, modelFileName: v.modelFileName, modelName: v.modelName+'-'+v.modelFileName, sampleRate: v.smplRate}, {modelPath: v.modelPath, modelFileName: v.lastModelFileName, projectSeq: v.projectSeq, snapshotSeq: v.snapshotSeq, modelId: v.modelId+'-'+v.lastModelFileName, modelName: v.modelName+'-'+v.lastModelFileName, sampleRate: v.smplRate}])
        }
        return arr;
      }, []).flat(3);
      this.modelListDisplay = lst;
      this.setTestModelList(lst);
    }
  }

  @action
  fetchTargetModelList = async (projectType: string) => {
    const _api = deepCopyObject(API.PROJECT_TEST_TARGET_MODEL_DATA_LIST);
    _api.url = _api.url.replace('_LANG_', projectType === 'STT' ? this.sttProjectStore.getLanguage() : this.lmProjectStore.getLanguage());
    _api.url = _api.url.replace('_ENGN_', projectType === 'LM' ? 'EN_STT' : 'LM');
    const res = await callApi(_api);
    console.log('get fetchTestTargetModelList');
    if (res.data !== undefined && res.data.length > 0) {
      const lst = res.data.reduce((arr, v) => {
        if(projectType === 'LM'){
          arr.push([{modelPath: v.modelPath, projectSeq: v.projectSeq, snapshotSeq: v.snapshotSeq, modelId: v.modelId+'-'+v.modelFileName, modelFileName: v.modelFileName, modelName: v.modelName+'-'+v.modelFileName, sampleRate: v.smplRate}, {modelPath: v.modelPath, modelFileName: v.lastModelFileName, projectSeq: v.projectSeq, snapshotSeq: v.snapshotSeq, modelId: v.modelId+'-'+v.lastModelFileName, modelName: v.modelName+'-'+v.lastModelFileName, sampleRate: v.smplRate}])
        }else{
          arr.push([{modelPath: v.modelPath, projectSeq: v.projectSeq, snapshotSeq: v.snapshotSeq, modelId: v.modelId+'-'+v.modelFileName, modelFileName: v.modelFileName, modelName: v.modelName+'-'+v.modelFileName, sampleRate: v.smplRate}])
        }
        return arr;
      }, []).flat(3);
      this.targetModelListDisplay = lst;
      this.setTestTargetModelList(lst);
    }
  }

  @observable modelTestSnapshotId : number = 1;
  @action getModelTestSnapshotId(){ return this.modelTestSnapshotId; }
  @action setModelTestSnapshotId(modelTestSnapshotId){ this.modelTestSnapshotId = modelTestSnapshotId; }

  @observable modelTestResult : string = '';
  @action getModelTestResult(){ return this.modelTestResult; }
  @action setModelTestResult(modelTestResult){ this.modelTestResult = modelTestResult; }

  // 모델 테스트 생성 요청
  @action
  fetchSttTest = async (src, mdId, mdlFileName, mdlFilePath) => {

    const formData = new FormData();
    formData.append('modelId', mdId);
    formData.append('modelFileName', mdlFileName);
    formData.append('modelFilePath', mdlFilePath);
    formData.append('dataFile', src, guid() + '.wav');

    const _api = deepCopyObject(API.MODEL_TEST_BEGIN);

    await callTestApi(_api, formData).then((res)=>{
      if(res.status === 200){
        this.setModelTestSnapshotId(res.data.modelTestSnapshotId);
        if(res.data.status === 'Waiting') {
          console.log('test : Waiting');
          console.log(res.data.queueIndex);
          this.root.modelTestStore.setQueueIndex(res.data.queueIndex);
          this.root.modelTestStore.setStepNum(3);
        } else if(res.data.status === 'Running') {
          console.log('Running');
          this.root.modelTestStore.setStepNum(4);
        } else if(res.data.status === 'Completed') {
          console.log('Completed');
          this.root.modelTestStore.setStepNum(5);
          this.setModelTestResult(res.data.result);
        } else if(res.data.status === 'Error') {
          console.log('Error');
          this.root.modelTestStore.setStepNum(6);
        }
        this.root.notiStore.infoGreen('테스트 시작');
        console.dir(res.data);
      }else{
        this.root.notiStore.error('테스트 시작 실패');
        this.root.modelTestStore.setStepNum(6);
        console.log('%c HTTP status : ' + res.status + ' :  test begin fail\n', res.data);
      }
    }).catch((res)=>{
      this.root.notiStore.error('테스트 시작 실패');
      this.root.modelTestStore.setStepNum(6);
      console.log('%c HTTP status : ' + res.status + ' :  test begin fail\n', res.data);
    })
  }

  // 상태 조회
  @action
  fetchSttTestStatus = async () => {
    const id = this.getModelTestSnapshotId();
    const params = {modelTestSnapshotId: id}
    const _api = deepCopyObject(API.MODEL_TEST_STATUS);


      await callApi(_api, params).then((res)=>{
        console.log(res.data.queurIndex);
        if(res.status === 200){
          console.log(res.data);
          if(res.data.status === 'Waiting') {
            console.log('status : Waiting');
            console.log(res.data.queueIndex);
            this.root.modelTestStore.setQueueIndex(res.data.queueIndex);
            this.root.modelTestStore.setStepNum(3);
          } else if(res.data.status === 'Running') {
            console.log('Running');
            this.root.modelTestStore.setStepNum(4);
          } else if(res.data.status === 'Completed') {
            console.log('Completed');
            this.root.modelTestStore.setStepNum(5);
            this.setModelTestResult(res.data.result);
            // clearInterval(interval);
          } else if(res.data.status === 'Error') {
            console.log('Error');
            this.root.modelTestStore.setStepNum(6);
            // clearInterval(interval);
          }
        }else{
          this.root.notiStore.error('테스트 status 호출 실패');
          this.root.modelTestStore.setStepNum(6);
          // clearInterval(interval);
          console.log('%c HTTP status : ' + res.status + ' :  test status fail\n', res.data);
        }
      }).catch((res)=>{
        this.root.notiStore.error('테스트 status 호출 실패');
        this.root.modelTestStore.setStepNum(6);
        // clearInterval(interval);
        console.log('%c HTTP status : ' + res.status + ' :  test status fail\n', res.data);
      })

  };

  // 실행 중인 모델 테스트 취소
  @action
  fetchSttTestCancel = async (id: number) => {
    console.log('get fetchSttTestCancel' + id);

    const param = {modelTestSnapshotId: id,}
    const _api = deepCopyObject(API.MODEL_TEST_CANCEL);
    const res = await callApi(_api, param);
    console.log(res.data);

    this.root.modelTestStore.setStepNum(1);
  }
}



