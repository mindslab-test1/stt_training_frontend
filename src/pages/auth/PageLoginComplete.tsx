import React from 'react';
import { ConLoginComplete } from 'containers/auth';
import { IContainerBaseProps } from 'constants/BaseInterface';

const PageLoginComplete: React.FC<IContainerBaseProps> = ({ ...props }) => {
  return <ConLoginComplete {...props} />;
};

export default PageLoginComplete;
