import React from 'react';
import { ConSsoLogin } from 'containers/auth';
import { IContainerBaseProps } from 'constants/BaseInterface';

const PageSsoLogin: React.FC<IContainerBaseProps> = ({ ...props }) => {
  return <ConSsoLogin {...props} />;
};

export default PageSsoLogin;
