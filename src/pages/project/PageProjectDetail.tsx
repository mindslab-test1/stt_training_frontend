import React, {useContext} from 'react';
import { IContainerBaseProps } from 'constants/BaseInterface';
import {getIdFromPath} from '../../utils/Navigation';
import SttLayout from '../../components/Project/stt/SttLayout';
import {ConProjectDetail} from '../../containers/project/ConProjectDetail';
import RootStore from '../../store/RootStore';
import {isLoggedIn} from '../../service/AuthService';
import {ERouteUrl} from '../../router/RouteLinks';
import LmLayout from "../../components/Project/lm/LmLayout";

let refreshCount = 0;

const PageProjectDetail: React.FC<IContainerBaseProps> = ({ ...props }) => {
  const {layoutStore} = useContext(RootStore);
  const { toggleHeader, toggleSidebar } = layoutStore;

  // Auth Ping Test;
  isLoggedIn()
    .then((res) => {
      if (!res) {
        props.history.replace(ERouteUrl.SSO_LOGIN);
      }
    })
    .catch(() => {
      props.history.replace(ERouteUrl.SSO_LOGIN);
    });

  toggleHeader(true);
  toggleSidebar(true);

  const id = getIdFromPath(location.pathname);
  return (
    <ConProjectDetail id={id} refreshCount={refreshCount++}>
        {(type) => {
            if(type === 'STT') {
                return (
                  <SttLayout {...props}/>
                );
            }
            else if(type === 'LM') {
              return (
                  <LmLayout {...props}/>
              );
            }
            else if(type === 'TTS') {
              return (<div/>);
            }
            else {
              return (<div/>);
            }
        }}
    </ConProjectDetail>
  );
};

export default PageProjectDetail;
