import React, {useContext} from 'react';
import ConModelApiList from '../../containers/project/ConModelApiList';
import {IContainerBaseProps} from '../../constants/BaseInterface';
import RootStore from '../../store/RootStore';
import {isLoggedIn} from '../../service/AuthService';
import {ERouteUrl} from '../../router/RouteLinks';


const PageModelList: React.FC<IContainerBaseProps> = ({...props}) => {

  const {layoutStore} = useContext(RootStore);
  const { toggleHeader, toggleSidebar } = layoutStore;

  // Auth Ping Test;
  isLoggedIn()
    .then((res) => {
      if (!res) {
        props.history.replace(ERouteUrl.SSO_LOGIN);
      }
    })
    .catch(() => {
      props.history.replace(ERouteUrl.SSO_LOGIN);
    });

  toggleHeader(true);
  toggleSidebar(true);

  return <ConModelApiList {...props} />
}

export default PageModelList;
