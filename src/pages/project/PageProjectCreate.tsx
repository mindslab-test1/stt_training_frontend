import React from 'react';
import ConProjectCreate from '../../containers/project/ConProjectCreate';
import { IContainerBaseProps } from 'constants/BaseInterface';

const PageProjectCreate: React.FC<IContainerBaseProps> = ({ ...props }) => {
    return <ConProjectCreate {...props} />;
};

export default PageProjectCreate;
