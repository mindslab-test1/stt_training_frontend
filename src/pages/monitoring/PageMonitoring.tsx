import React, {useContext} from 'react';
import {IContainerBaseProps} from '../../constants/BaseInterface';
import RootStore from '../../store/RootStore';
import {isLoggedIn} from '../../service/AuthService';
import {ERouteUrl} from '../../router/RouteLinks';
import {ComPage} from "../../components/commons";
import ConServiceMonitoring from "../../containers/monitoring/ConServiceMonitoring";
import ConDataProcessingMonitoring from "../../containers/monitoring/ConDataProcessingMonitoring";


const PageModelList: React.FC<IContainerBaseProps> = ({...props}) => {

  const {layoutStore} = useContext(RootStore);
  const { toggleHeader, toggleSidebar } = layoutStore;

  // Auth Ping Test;
  isLoggedIn()
    .then((res) => {
      if (!res) {
        props.history.replace(ERouteUrl.SSO_LOGIN);
      }
    })
    .catch(() => {
      props.history.replace(ERouteUrl.SSO_LOGIN);
    });

  toggleHeader(true);
  toggleSidebar(true);

  return (
      <>
          <ConServiceMonitoring />
          <ConDataProcessingMonitoring />
      </>
  );
}

export default PageModelList;
