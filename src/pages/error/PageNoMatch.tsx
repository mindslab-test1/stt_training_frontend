import React, {useContext} from 'react';
import {IContainerBaseProps} from '../../constants/BaseInterface';
import RootStore from '../../store/RootStore';
import 'assets/css/error.css';

const PageNoMatch: React.FC<IContainerBaseProps> = ({...props}) => {
  const {layoutStore, userStore} = useContext(RootStore);
  const { toggleHeader, toggleSidebar } = layoutStore;

  toggleHeader(false);
  toggleSidebar(false);
  // <>
  //   <img className='errorImg' src={errImg} alt='error Image'/>
  // </>
  return (
    <div className='errorWrap'>
    <div className='errorBox'>
      <div className='error_tit'>
        <strong>죄송합니다.<br/>잘못된 접근입니다.</strong>
      </div>
      <div className='error_cnt'>
        <p>일시적인 현상으로, 잠시 후 다시 이용해 보시면 정상 접속될 수 있습니다.</p>
        <p>담당부서에서 확인중이나, 문제가 계속되는 경우 접속 오류에 대해<br/><a href='mailto:hello@mindslab.ai'>고객센터</a>로 연락 부탁 드립니다.</p>
        <p>이용에 불편을 드려 다시 한번 사과 드립니다.</p>
      </div>
      <div className='error_btn'>
        <a className='btn_clr' href='/'>홈으로 이동</a>
      </div>
    </div>
    </div>
  );
};

export default PageNoMatch;
