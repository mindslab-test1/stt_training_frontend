import React, {useEffect, useRef, useState} from 'react';
import {Link} from 'react-router-dom';

interface Props {
  title: string;
  clsName: string;
  children: React.ReactNode[] | React.ReactNode;
}

const ComToggleBox: React.FC<Props> = ({title, clsName, children}) => {
  const [isActive, setActive] = useState(false);

  const ref = useRef(null);

  const toggleClass = (e) => {
    // debugger
    if(e.target.parentNode === ref.current) {
      setActive(!isActive);
    }
    else {
      setActive(false);
    }
  }

  useEffect(() => {
    document.addEventListener('click', toggleClass, true);
    return () => {
      document.removeEventListener('click', toggleClass, true);
    };
  });

  return (
    <li ref={ref} className={isActive ? 'active' : ''}>
      <a className={'btn_ico ' + clsName} href={void(0)}>{title}</a>
      {children}
    </li>
  );
}

export default ComToggleBox;
