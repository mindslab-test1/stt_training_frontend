import React, { useContext } from 'react';
import { observer } from 'mobx-react-lite';
import { Link } from 'react-router-dom';
import { ERouteUrl } from 'router/RouteLinks';
import RootStore from '../../store/RootStore';

// const { Sider } = Layout;
// const { SubMenu } = Menu;

interface Props {}

// let key = 1;

const Sidebar: React.FC<Props> = () => {
  const {layoutStore} = useContext(RootStore);
  const { showSidebar } = layoutStore;

  if (showSidebar) {
    return (
      <div className='snb'>
        <p className='bgBox'>
          <Link to={ERouteUrl.PROJECT_CREATE}><span className='fas fa-plus' />프로젝트 시작하기</Link>
        </p>
        <div className='navBox'>
          <span className='nav_tit'>내 작업</span>
            <ul className='nav'>
              <li><Link to={ERouteUrl.PROJECT_LIST}><span className='fab fa-font-awesome-flag' />진행중인 프로젝트</Link></li>
              <li><Link to={ERouteUrl.MONITORING}><span className='fas fa-desktop' />모니터링 현황</Link></li>
              {/*<li><Link to={ERouteUrl.MODEL_API}><span className='fas fa-rocket' />모델 API</Link></li>*/}
            </ul>
        </div>
      </div>
    );
  } else {
    return <div />;
  }
};

export default observer(Sidebar);
