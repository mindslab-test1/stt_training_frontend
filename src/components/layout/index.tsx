import _Footer from './Footer';
import _Header from './Header';
import _Sidebar from './Sidebar';

export const Footer = _Footer;
export const Header = _Header;
export const Sidebar = _Sidebar;
