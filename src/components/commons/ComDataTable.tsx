import React, {useContext} from 'react';
import {observer} from 'mobx-react-lite';
import RootStore from "../../store/RootStore";

interface Props {
  columns: any;
  data: any;
  title: string;
}

const ComDataTable: React.FC<Props> = ({title, columns, data}) => {

  const {sttProjectStore, lmProjectStore, legacyDataStore, notiStore} = useContext(RootStore);
  let fileRef = React.createRef<HTMLInputElement>();

  const onClick = () => {
    fileRef.current?.click();
  }

  const onChange = (e: React.FormEvent<HTMLInputElement>) => {
    legacyDataStore.setLearnData(e.currentTarget.files);
  }

  return (
    <dd className='tbl_lstBox scroll_s'>
      <table id='srch_dataLst' className='tbl_multiSelect'>
        <caption className='hide'>{title}</caption>
        <thead>
          <tr>
            {columns.map(
              (column, index) => <th scope='col' key={index}>{column.title}</th>
            )}
          </tr>
        </thead>
        <tbody>
        {
          (data && data.length > 0) ?
            data.map(
              (item, index) => (
                <tr key={index}>
                  {columns.map(({dataIndex, displayFtn}, index) => <td key={index}> {displayFtn(item[dataIndex])} </td> )}
                </tr>
              )
            )
          :
            <tr id='srchDataNone'>
              <td scope='row' style={{cursor: 'pointer'}} onClick={onClick} colSpan={2} className='dataNone'>
                업로드 할 파일이 없습니다.
                <input type='file' style={{display: 'none'}} onChange={onChange} ref={fileRef} accept={lmProjectStore.projectType === 'STT' ? '.wav, .txt' : '.txt'} multiple/>
              </td>
            </tr>
        }
        </tbody>
      </table>
    </dd>
  );
};

export default observer(ComDataTable);
