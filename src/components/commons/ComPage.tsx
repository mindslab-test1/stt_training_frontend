import React, {memo} from 'react';


interface Props {
  title: string;
  subTitle: string;
  engineName : string;
  isVisibleSpan : boolean;
  children: React.ReactNode[] | React.ReactNode;
}

const ComPage: React.FC<Props> = ({ title,subTitle, isVisibleSpan, engineName, children}) => {

    return (
        <>
            <div className='titArea'>
                <h3>{title}{isVisibleSpan && <span className='en_type ft_clr_stt'>{engineName}</span>}</h3>
                {subTitle !== '' ? <p>{subTitle}</p> : null}
            </div>
            {children}
        </>
    );

};




export default ComPage;
