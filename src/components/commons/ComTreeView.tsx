import React from 'react';

interface Props {
  data: any[];
  selectedGroup: string;
  selectedMember: string;
   onClick: (fileName: any, mdlId: any, sampleRate) => void;
}

console.log(this.data);

const ComTreeView: React.FC<Props> = ({data, onClick, selectedGroup, selectedMember}) => {
  return (
    <>
      {
        (data && data.length > 0) ?
          Array.from(data).map((item, index) => {
            return (
              <li key={index} onClick={() => onClick(item.modelName, item.modelId, item.sampleRate)}><span className={(item.modelId === selectedMember) ? 'file active' : 'file'}>{item.modelName}</span>
              </li>
            )
          })
        :
          <li><span className='file'>모델 정보가 없습니다.</span></li>
      }
    </>
  );
};

export default ComTreeView;
