import React from 'react';

interface Props {
    title: string;
    onClick: (e) => void;
    disabled : boolean;
}

const ComNextBtn: React.FC<Props> = ({title, onClick,disabled}) => {

    return (
        <div className='cell_btm'>
            <div className='btnSqBox'>
                <button type='button' disabled={disabled} onClick={onClick}>{title}</button>
            </div>
        </div>
    );
};

export default ComNextBtn;
