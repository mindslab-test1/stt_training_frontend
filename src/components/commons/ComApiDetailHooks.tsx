import React, { useEffect, useState } from 'react';
import { deepCopyObject } from 'utils/CommonUtils';
import { callApi, API, IApiDefinition } from 'service/ApiService';

interface Params {
  id: number | string;
  refreshCount?: number;
  children: any;
}

const useGetResultApi = (api: IApiDefinition, urlKey: string, urlId: any, _refreshCount: number | undefined) => {
  const [refreshCount, setRefreshCount] = useState(1);
  if (_refreshCount && _refreshCount !== refreshCount) {
    setRefreshCount(_refreshCount);
  }

  const [data, setData] = useState(null);
  useEffect(() => {
    if (urlKey && urlId) {
      api = deepCopyObject(api);
      api.url = api.url.replace(urlKey, urlId);
    }

    async function fetchData() {
      const res = await callApi(api);
      if (res.success) {
        setData(res.data);
      }
    }

    void fetchData();
  }, [refreshCount]);
  return data;
};


// export const ComApiStoreDetail = ({ id, refreshCount, children }: Params) => {
//   if (!id || !children) return null;
//   const detail = useGetResultApi(API.STORE_DETAIL, '_ID_', id, refreshCount);
//   return children(detail);
// };
