import React from 'react';
import ReactDOM from 'react-dom';


interface Props {
    children:  React.ReactNode[] | React.ReactNode;
    show: boolean;
}

const ComPopBox: React.FC<Props> = ({children, show}) => {

    if(show){

        const el = document.getElementById('modal');
        // @ts-ignore
        return ReactDOM.createPortal(children, el);
    }
    return null;

};

export default ComPopBox;
