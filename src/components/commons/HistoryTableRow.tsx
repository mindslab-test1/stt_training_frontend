import React from 'react';
import HistoryUseDataRow from './HistoryUseDataRow';
import HistoryNoiseRow from './HistoryNoiseRow';

interface Props {
    item : any;
    onClick: (id)=>void;
    selectedRowId:number;
}

const HistoryTableRow: React.FC<Props> = ({item,onClick,selectedRowId }) => {

  const snapshot = JSON.parse(item.snapshot);
  let dataList = snapshot.learningDataList;
  let noiseList = snapshot.noiseDataList;

  let dataTblWidth = 70 + (60 * dataList.length);
  let noiseTblWidth = 70 + (60 * noiseList.length);

  return(

      <tr onClick={()=>onClick(item.id)} className={selectedRowId === item.id ? 'active' : ''} id={item.id}>
        <td scope='row' className='txt_ellipsis'>{item.name}</td>
        <td>
            <div className='hiddenBox' >
                <dl className='dlBox'>
                    <dt>언어</dt>
                    <dd>{snapshot.language === 'LN_KO' ? '한국어' : '영어'}</dd>
                </dl>
                <dl className='dlBox'>
                    <dt>샘플링레이트</dt>
                    <dd>{snapshot.sampleRate === 'SR001' ? '8K' : '16K'}</dd>
                </dl>
                <dl className='dlBox'>
                    <dt>Use EOS</dt>
                    <dd>{snapshot.eos}</dd>
                </dl>
                <h3>학습에 사용할 데이터</h3>
                <div className='tbl_lstBox scroll_s'>
                    <table style={{width:dataTblWidth}}>
                        <caption className='hide'>학습에 사용할 데이터 목록</caption>
                        <colgroup>
                            <col /><col /><col /><col /><col />
                        </colgroup>
                        <thead>
                        <tr>
                            <th scope='col'>Name</th>
                            <th scope='col'>샘플링레이트</th>
                            <th scope='col'>전체음성길이</th>
                            <th scope='col'>평균음성길이</th>
                            <th scope='col'>EOS 여부</th>
                        </tr>
                        </thead>
                        <tbody>
                        {dataList.map((dataItem,index)=>((
                            <HistoryUseDataRow key={index} dataItem={dataItem} />
                        )))}
                        </tbody>
                    </table>
                </div>
            </div>
        </td>
        <td>
            <div className='hiddenBox' >
                <h3>기본설정</h3>
                <dl className='dlBox'>
                    <dt>Epoch</dt>
                    <dd>{snapshot.epochSize}</dd>
                </dl>
                <dl className='dlBox'>
                    <dt>Batch Size</dt>
                    <dd>{snapshot.batchSize}</dd>
                </dl>
                <h3>노이즈설정</h3>
                <div className='tbl_lstBox scroll_s'>
                    <table style={{width:noiseTblWidth}}>
                        <caption className='hide'>노이즈 데이터 목록</caption>
                        <colgroup>
                            <col /><col /><col /><col /><col />
                        </colgroup>
                        <thead>
                        <tr>
                            <th scope='col'>Name</th>
                            <th scope='col'>샘플링레이트</th>
                            <th scope='col'>전체음성길이</th>
                            <th scope='col'>평균음성길이</th>
                        </tr>
                        </thead>
                        <tbody>
                        {noiseList.map((noiseItem,index)=>((
                            <HistoryNoiseRow key={index} noiseItem={noiseItem} />
                        )))}
                        </tbody>
                    </table>
                </div>
                <dl className='dlBox'>
                    <dt>Min noise sample</dt>
                    <dd>{snapshot.minNoiseSample}</dd>
                </dl>
                <dl className='dlBox'>
                    <dt>Max noise sample</dt>
                    <dd>{snapshot.maxNoiseSample}</dd>
                </dl>
                <dl className='dlBox'>
                    <dt>Min SNR</dt>
                    <dd>{snapshot.minSnr}</dd>
                </dl>
                <dl className='dlBox'>
                    <dt>Max SNR</dt>
                    <dd>{snapshot.maxSnr}</dd>
                </dl>
            </div>
        </td>
        <td>
            <div className='hiddenBox'>
                <h3>Best Score</h3>
                <dl className='dlBox'>
                    <dt>Token Error Rate</dt>
                    <dd>{item.bestTerEpoch} epoch, {item.bestTer}%</dd>
                </dl>
                <dl className='dlBox'>
                    <dt>Word Error Rate</dt>
                    <dd>{item.bestWerEpoch} epoch, {item.bestWer}%</dd>
                </dl>
            </div>
        </td>
        <td>{item.regDt.replace(/T/g,' ')}</td>
    </tr>
  )
}

export default React.memo(HistoryTableRow);
