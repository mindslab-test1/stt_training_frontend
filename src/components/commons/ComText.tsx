import styled from 'styled-components';

interface TextProps {
  align?: 'center' | 'left' | 'right';
  weight?: 300 | 400 | 600 | 800;
  size?: '10px' | '12px' | '14px' | '16px' | '18px' | '20px' | '30px';
}

export enum EColor {
  // DEFAULT = 'rgba(255, 255, 255, 0.65)',
  DEFAULT = 'rgba(0, 0, 0, 0.65)',
  WHITE = '#FFF',
  GREY = '#c8c8c8',
  MAIN_COLOR = '#9789F5',
  RED = 'red',
  LINK = 'skyblue',
}

export const Text = styled.p<TextProps>`
  color: ${(props) => props.color || EColor.DEFAULT};
  /* font-size: 1em; */
  font-size: ${(props) => props.size || '14px'};
  font-weight: ${(props) => props.weight || 400};
  text-align: ${(props) => props.align || 'left'};
  margin: 0;
`;

export const TextLink = styled.a`
  font-size: 14px;
  text-decoration: underline;
  color: rgb(34, 153, 153, 1);
  cursor: pointer;

  :hover {
    color: rgb(34, 153, 153, 0.5);
  }
`;

export const Text12 = styled(Text)`
  font-size: 12px;
`;

export const Text14 = styled(Text)`
  font-size: 14px;
`;

export const Text16 = styled(Text)`
  font-size: 16px;
`;

export const Text20 = styled(Text)`
  font-size: 20px;
`;

export const Text24 = styled(Text)`
  font-size: 24px;
`;
