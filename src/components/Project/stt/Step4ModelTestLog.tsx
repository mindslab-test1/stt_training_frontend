import React, {useContext, useEffect, useState} from 'react';
import {observer} from 'mobx-react-lite';
import RootStore from "../../../store/RootStore";
import SockJsClient from 'react-stomp';

interface Props {

}

const Step4ModelTestLog : React.FC<Props> = () => {

    const {modelTestStore} = useContext(RootStore);
    const [logMessage, setLogMessage] = useState(new Array());
    const downloadLog = () => {
        const start = logMessage.findIndex((v) => v.indexOf('------ Syl INFO ------') > -1);
        const end = logMessage.findIndex((v) => v.indexOf('complete') > -1);
        const syl = logMessage.splice(start+1, end-2);
        const element = document.createElement("a");
        const file = new Blob(syl, {type: 'text/plain'});
        element.href = URL.createObjectURL(file);
        element.download = modelTestStore.currentModel.modelName+"_syl.txt";
        document.body.appendChild(element);
        element.click();
    }
    useEffect(()=>{

    },[]);
    return (
        <>
            <div style={{overflowY: "auto", maxHeight: '300px'}}>
                {
                    logMessage.map((message, index) => <p key={index}>{message}</p>)
                }
            </div>
            {
                logMessage.map((message, index) => {
                    if(message.indexOf('complete') > -1){
                        return <button key={index} onClick={downloadLog}>결과 다운로드</button>
                    }
                })
            }
            <SockJsClient url='/ws'
                          topics={['/ws/subscribe/test/'+modelTestStore.currentModel.projectSeq+'/'+modelTestStore.currentModel.snapshotSeq]}
                          onMessage={(msg) => setLogMessage(logMessage.concat([msg]))}
            />
        </>
    );
}

export default observer(Step4ModelTestLog);
