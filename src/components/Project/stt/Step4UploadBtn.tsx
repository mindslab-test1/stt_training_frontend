import React, {useContext, useEffect, useState} from 'react';
import RootStore from '../../../store/RootStore';

interface Props {
    disb:boolean;
    testReady:boolean;
    targetDisb:boolean;
}

const Step4UploadBtn : React.FC<Props> = ({disb, testReady, targetDisb}) => {
    const {modelTestStore, sttProjectStore, notiStore} = useContext(RootStore);
    let style = !disb && !targetDisb ? 'ico_upload' : 'ico_upload disabled';
    let testStyle = testReady ? 'ico_upload disabled' : 'ico_upload';

    const onChange = () => {
        // @ts-ignore
        let file = document.getElementById('ipt_file').files;
        sttProjectStore.uploadData(4, file);
    }

    const startModelTest = () => {
        if(sttProjectStore.getTestingDataList().length === 0) {
            notiStore.warning('테스트 할 파일을 등록해주세요.');
        } else {
            sttProjectStore.startModelTest(
                sttProjectStore.getTestingDataList().reduce((arr, v) => {
                    arr = arr.concat(v.files);
                    return arr;
                }, new Array<File>())
            );
        }
    }

    return (
        <>
            <div className='fileBox'>
                <label htmlFor='ipt_file' className={style}>파일 업로드</label>
                <label className={testStyle} onClick={startModelTest}>평가하기</label>
                <input type='file' id='ipt_file' accept=".wav, .txt" multiple onChange={onChange}/>
            </div>

        </>
    );
}

export default Step4UploadBtn;