import React, {useContext} from 'react';
import RootStore from '../../../store/RootStore';
import ComTreeView from '../../commons/ComTreeView';
import {observer} from 'mobx-react-lite';

interface Props {
  disb:any;
}

const Step4TargetModelTreeView : React.FC<Props> = ({disb}) => {
  const {modelTestStore} = useContext(RootStore);
  const testModel = modelTestStore.getTargetModelList;
  const selectedFileName = (modelTestStore.currentTargetModel && modelTestStore.currentTargetModel.modelName)
    ? modelTestStore.currentTargetModel.modelId : '';
  const selectedModelName = (modelTestStore.currentTargetModel && modelTestStore.currentTargetModel.modelId)
    ? modelTestStore.currentTargetModel.modelId : '';

  const onClick = (name, id, sampleRate) => {
    // console.log(name);
    modelTestStore.setCurrentTargetModel(name, id);
    disb(false);
  }

  return (
    <dd className='treeBox scroll_s' id='treeSelect'>
      <ul className='filetree'>
        <ComTreeView data={testModel} selectedMember={selectedFileName} selectedGroup={selectedModelName} onClick={onClick} />
      </ul>
    </dd>
  );
};

export default observer(Step4TargetModelTreeView);
