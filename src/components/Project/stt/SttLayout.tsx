import React, {useContext, useEffect, useState} from 'react';
import {IContainerBaseProps} from 'constants/BaseInterface';
import {ComPage} from 'components/commons';
import {getIdFromPath} from 'utils/Navigation';
import ComStepBox from '../../commons/ComStepBox';
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import Step4 from './Step4';
import RootStore from '../../../store/RootStore';
import {observer} from 'mobx-react-lite';

interface Props extends IContainerBaseProps{
}

const SttLayout: React.FC<Props> = ({location}) => {
  const id = getIdFromPath(location.pathname);
  const {sttProjectStore} = useContext(RootStore);

  useEffect( () => {
  })

  const [step2Update, setStep2Update] = useState(false);

  const StepBoxTextList = [
    {
      id: 0,
      stepNum:'01',
      stepStr:'01',
      stepTitle:'데이터 선택',
      stepDesc:'학습 할 데이터를 조회 및 선택합니다.',
      childComponent : <Step1 id={0}/>,
    },
    {
      id: 1,
      stepNum:'02',
      stepStr:'02',
      stepTitle:'학습 설정 & 시작',
      stepDesc:'학습 데이터의 세부설정이 가능하며 사전학습모델 선택하고 학습 시작할 수 있습니다.',
      setUpdate: () => setStep2Update,
      childComponent : <Step2 id={1}/>,
    },
    {
      id: 2,
      stepNum:'03',
      stepStr:'03',
      stepTitle:'학습 모니터링',
      stepDesc:'학습 진행상황을 모니터링하며, 학습 모델을 생성합니다.',
      childComponent : <Step3 id={2}/>,
    },
    {
      id: 3,
      stepNum:'04',
      stepStr:'04',
      stepTitle:'학습 결과확인 & 테스트',
      stepDesc:'학습이 완료된 모델과 이력을 확인 하실 수 있습니다. 테스트 및 엔진 API 생성이 가능합니다.',
      childComponent : <Step4 id={4}/>,
    }
  ]

  return (
    <ComPage title={sttProjectStore.projectName} subTitle={sttProjectStore.projectDescription} isVisibleSpan={true} engineName={sttProjectStore.projectType}>
      <div className='cellArea'>
        {StepBoxTextList.map((stepBox, index) => (
          <ComStepBox
              key={index}
              id={stepBox.id}
              stepNum={stepBox.stepNum}
              stepStr={stepBox.stepStr}
              stepTitle={stepBox.stepTitle}
              stepDesc={stepBox.stepDesc}>
            {stepBox.childComponent}
          </ComStepBox>
        ))}
      </div>
    </ComPage>
  );

};

export default observer(SttLayout);
