import React, {useContext} from 'react';
import RootStore from '../../../store/RootStore';
import ComChartLine from '../../commons/ComChartLine';
import {observer} from 'mobx-react-lite';

interface Props {
}

const Step3Training : React.FC<Props> = () => {

  const {trainingStore, sttProjectStore} = useContext(RootStore);
  const dataPoints = trainingStore.dataPoints;

  let lossData = new Map();
  let terData = new Map();
  let werData = new Map();
  let categories: number[] = [];

  if (dataPoints !== undefined && Array.isArray(dataPoints) && dataPoints.length > 0) {
    for (const _items of dataPoints) {
      const items = _items['data'];
      const epoch = _items['epoch'];
      categories.push(epoch);

      for (const item of items) {
        let currentData;
        if (item.graphType === 'loss') {
          currentData = lossData;
        }
        else if (item.graphType === 'TER') {
          currentData = terData;
        }
        else if (item.graphType === 'WER') {
          currentData = werData;
        }

        for (const data of Object.entries(item)) {
          const key = data[0];
          const val = data[1];

          if (key !== 'graphType') {
            if(currentData.get(key) === undefined) {
              currentData.set(key, []);
            }
            currentData.get(key).push(val);
          }
        }
      }
    }
  }

  return (
      <div className='fifo_none'>
        <div className='lot_item'>
          <dl>
            <dt>학습모델 아이디
              <span className='ico_help'><em>학습모델의 고유코드입니다.</em></span>
            </dt>
            <dd>{trainingStore.modelId}</dd>
            {/* [D] STT학습모델 아이디 규칙: 사용자ID_학습데이터Type_생성일시분_엔진type_샘플링레이트 userId_voice_20200916130221_STT_kor_16k*/}
          </dl>
        </div>
        <div className='lot_item'>
          <dl>
            <dt>직전 epoch 처리시간
              <span className='ico_help'><em>직전 실행된 epoch의 처리시간입니다.</em></span>
            </dt>
            <dd>{trainingStore.timePerSession || '-'}</dd>
          </dl>
        </div>
        <div className='lot_item'>
          <dl>
            <dt>Loss
              <span className='ico_help'><em>학습 모니터링 오류 비율입니다.</em></span>
            </dt>
            <dd className='chartBox'>
              <ComChartLine categories={categories} graphData={lossData}/>
            </dd>
          </dl>
          <dl>
            <dt>Token Error Rate
              <span className='ico_help'><em>학습 모니터링에서 토큰 단위로 오류가 있는 비율입니다.<br />*토큰(Token)이란 문법적으로 더 이상 나눌 수 없는 언어 요소</em></span>
            </dt>
            <dd className='chartBox'>
              <ComChartLine categories={categories} graphData={terData}/>
            </dd>
          </dl>
        </div>
        <div className='lot_item'>
          <dl>
            <dt>Word Error Rate
              <span className='ico_help'><em>학습 모니터링에서 단어 단위로 오류가 있는 비율입니다.</em></span>
            </dt>
            <dd className='chartBox'>
              <ComChartLine categories={categories} graphData={werData}/>
            </dd>
          </dl>
          <dl/>
        </div>
      </div>
  );
}

export default observer(Step3Training)

;
