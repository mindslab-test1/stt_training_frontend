import React, {useContext} from 'react';
import {observer} from 'mobx-react-lite';
import RootStore from "../../../store/RootStore";

interface Props {
  columns: any;
  data: any;
  title: string;
  disb: boolean;
}

const Step4ModelTestDataTable: React.FC<Props> = ({title, columns, data, disb}) => {

  const {sttProjectStore, legacyDataStore, notiStore} = useContext(RootStore);
  let fileRef = React.createRef<HTMLInputElement>();

  const onClick = () => {
    fileRef.current?.click();
  }

  const onChange = (e: React.FormEvent<HTMLInputElement>) => {
    legacyDataStore.setLearnData(e.currentTarget.files);
  }

  return (
    <dd className='tbl_lstBox scroll_s'>
      <table id='srch_dataLst' className='tbl_multiSelect'>
        <caption className='hide'>{title}</caption>
        <thead>
          <tr>
            {columns.map(
              (column, index) => <th scope='col' key={index}>{column.title}</th>
            )}
          </tr>
        </thead>
        <tbody>
        {
          (data && data.length > 0) ?
            data.map(
              (item, index) => (
                <tr key={index}>
                  {columns.map(({dataIndex}, index) => <td key={index}> {item[dataIndex]} {dataIndex === 'duration' ? '초' : ''} </td> )}
                </tr>
              )
            )
          :
            <tr id='srchDataNone'>
              <td scope='row' style={{cursor: 'pointer'}} onClick={onClick} colSpan={columns.length} className='dataNone'>
                {disb ? '학습 모델을 선택해주세요.' : '테스트 할 파일이 없습니다.'}
              </td>
            </tr>
        }
        </tbody>
      </table>
    </dd>
  );
};

export default observer(Step4ModelTestDataTable);
