import React, {useContext} from 'react';
import {observer} from 'mobx-react-lite';
import RootStore from '../../../store/RootStore';
import {smplRateDisplay} from "../../../utils/CommonUtils";

interface Props {
    onClick: (id) => void;
    columns: any;
    data: any;
    title: string;
    stepId:number;
}

const Step1SelectedLearningData: React.FC<Props> = ({title, columns, data, onClick,stepId}) => {

   const {sttProjectStore} = useContext(RootStore);

   let dataTblWidth;
    if(stepId !== sttProjectStore.currentStep){
        dataTblWidth = 90 + (60 * data.length)+'px';
    }else {
        dataTblWidth = '100%';
    }

    return (
        <dd id='use_dataLst'  className='tbl_lstBox scroll_s'>
            <table className='tbl_multiSelect' style={{width:dataTblWidth}}>
                <caption className='hide'>{title}</caption>
                <thead>
                <tr>
                    {columns.map(
                        (column, index) => <th scope='col' key={index}>{column.title}</th>
                    )}
                </tr>
                </thead>
                <tbody>
                {
                    (data && data.length > 0) ?
                        data.map(
                            (item, index) => {
                                return (
                                    <tr onClick={() => onClick(item.atchFileId)} className='active' key={index}>
                                        {columns.map(({dataIndex, displayFtn}, index) => <td key={index}> {dataIndex === 'smplRate' ? smplRateDisplay(item[dataIndex]) : item[dataIndex]} </td> )}
                                    </tr>
                                )
                            }
                        )
                        :
                        <tr id='srchDataNone'>
                            <td scope='row' colSpan={5} className='dataNone'>사용 가능한 데이터가 없습니다.</td>
                        </tr>
                }
                </tbody>
            </table>
        </dd>
    );
};

export default observer(Step1SelectedLearningData);
