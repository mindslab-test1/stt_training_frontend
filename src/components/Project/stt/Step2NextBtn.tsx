import React, {useContext} from 'react';
import RootStore from '../../../store/RootStore';
import {observer} from 'mobx-react-lite';
import ComNextBtn from '../../commons/ComNextBtn';

interface Props {
    title: string;
    onClick: (e) => void;
}

const Step2NextBtn: React.FC<Props> = ({title, onClick}) => {

    const {sttProjectStore} = useContext(RootStore);

    // TODO : 추가적으로 제한 조건이 필요한지 확인 후 disable 세팅 해주기!
    const btnState = !(sttProjectStore.noiseDataList && sttProjectStore.noiseDataList.length !== 0); // (!sttProjectStore.getStep2Changed());

    return ( <ComNextBtn title={title} onClick={onClick} disabled={btnState}/> );
};

export default observer(Step2NextBtn);
