import React, {useContext} from 'react';
import ComDataTable from '../../commons/ComDataTable';
import RootStore from '../../../store/RootStore';
import {observer} from 'mobx-react-lite';
import {defaultDisplay, smplRateDisplay} from '../../../utils/CommonUtils';

interface Props {
  // onClick: (e) => void;
}

const LIST_COLUMNS = [
  { title: 'FILE NAME', dataIndex: 'name', displayFtn: defaultDisplay },
];

const Step1AvailableLearningDataTable: React.FC<Props> = () => {
  // console.log('in ComAvailableLearningDataTable');
  const {sttProjectStore, legacyDataStore} = useContext(RootStore);

  const onClick = (id) => {
    console.log('ComAvailableLearningDataTable onClick:' + id);
    const item = legacyDataStore.learningDataList.find(it => it.atchFileId === id);
    sttProjectStore.putLearningData(item);
  }

  return (
      <ComDataTable title={'업로드 할 데이터 목록'} columns={LIST_COLUMNS} data={legacyDataStore.getAvailLearnData}/>
  );
};

export default observer(Step1AvailableLearningDataTable);
