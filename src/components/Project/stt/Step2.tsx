import React, {useContext, useEffect, useState} from 'react';
import ComHelpIcon from '../../commons/ComHelpIcon';
import ComSelect from '../../commons/ComSelect';
import RootStore from '../../../store/RootStore';
import ComInput from '../../commons/ComInput';
import Step2ToggleBox from './Step2ToggleBox';
import NoiseDataTable from './Step2NoiseDataTable';
import {observer} from 'mobx-react-lite';
import ComPopBox from '../../commons/ComPopBox';
import PopSnapShot from '../../popup/PopSnapShot';
import Step2NextBtn from './Step2NextBtn';
import Step2PreTrainedModelTreeView from './Step2PreTrainedModelTreeView';

interface Props {
    id : number;
}

const Step2 : React.FC<Props> = ({id}) => {

    const {sttProjectStore,  notiStore} = useContext(RootStore);
    const [modelRegPopShow, setModelRegPopShow] = useState(false);

    let epochSize = [
        { epoch : 1}, { epoch : 2}, { epoch : 3}, { epoch : 4}, { epoch : 5},
        { epoch : 6}, { epoch : 7}, { epoch : 8}, { epoch : 9}, { epoch : 10}
    ]
    const batchSize = [
        { batch : 1}, { batch : 2}, { batch : 3}, { batch : 4}, { batch : 5},
        { batch : 6}, { batch : 7}, { batch : 8}, { batch : 9}, { batch : 10}
    ]

    const saveTrainingData = () => {
        sttProjectStore.setCheckSave(1);
        console.log('--------');
        console.log(sttProjectStore.getCheckSave());

        if(validateEpochSize() && validateBatchSize() && validateValidationRate() && validateSampleLength() && validateMinMaxNoiseSample() && validateMinMaxSNR()){
            void sttProjectStore.saveTrainSetting(setModelRegPopShow, modelRegPopShow);
        }else{
            console.log('%c Invalid input value!!', 'color:red');
        }
    }

    const onEpochSizeChange = (e) => { sttProjectStore.setEpochSize(parseInt(e.target.value, 10)); }
    const validateEpochSize = () => {
        let epochSize = sttProjectStore.getEpochSize();
        console.log('%c epochSize : ' + epochSize, 'color: green');
        if(epochSize < 1 || 10 < epochSize){
            notiStore.warning('Epoch 값을 선택해 주세요.');
            return false;
        }
        return true;
    }

    const onBatchSizeChange = (e) => {
        sttProjectStore.setBatchSize(parseInt(e.target.value, 10));
        console.log(sttProjectStore.getSampleRate());
        sttProjectStore.getSampleRate() === 'SR001' ? sttProjectStore.setMaxNoiseSample(Math.floor(4800000/e.target.value)) : sttProjectStore.setMaxNoiseSample(Math.floor(9600000/e.target.value));
        sttProjectStore.setSampleLength(Math.floor(600/e.target.value));
    }

    const validateBatchSize = () => {
        let batchSize = sttProjectStore.getBatchSize();
        console.log('%c batchSize : ' + batchSize, 'color: green');
        if(batchSize < 1 || 10 < batchSize){
            notiStore.warning('Batch size 값을 선택해 주세요.');
            return false;
        }
        return true;
    }

    const onValidationRateChange = (e) => { sttProjectStore.setRate(parseFloat(e.target.value)); }
    const validateValidationRate =  () => {
        let validationRate = sttProjectStore.getRate();
        console.log('%c validationRate : ' + validationRate, 'color: green');
        if(validationRate < 0 || 100 < validationRate){
            notiStore.warning('Validation rate 값이 올바르지 않습니다.');
            return false;
        }
        return true;
    }

    const onSampleLengthInputChange = (e) => { sttProjectStore.setSampleLength(parseInt(e.target.value, 10)); }
    const validateSampleLength = () => {
        let sampleLength = sttProjectStore.getSampleLength();
        console.log('%c sampleLength : ' + sampleLength, 'color: green');
        if(sampleLength < 0){
            notiStore.warning('음성길이 값이 올바르지 않습니다.');
            return false;
        }
        return true;
    }


    const validateMinMaxNoiseSample = () => {
        let minNoise = sttProjectStore.getMinNoiseSample();
        let maxNoise = sttProjectStore.getMaxNoiseSample();
        if(minNoise > maxNoise){
            notiStore.warning('Min noise sample 값은 Max 값보다 클 수 없습니다.');
            return false;
        }
        return true;
    }

    const validateMinMaxSNR = () => {
        let minSNR = sttProjectStore.getMinSnr();
        let maxSNR = sttProjectStore.getMaxSnr();
        if(minSNR > maxSNR){
            notiStore.warning('Min SNR 값은 Max 값보다 클 수 없습니다.');
            return false;
        }
        return true;
    }

    const closePopup = () => {
        setModelRegPopShow(false);
    }


    useEffect(()=>{
        if(sttProjectStore.checkSave === 0){
            sttProjectStore.setEpochSize(1);
            sttProjectStore.setBatchSize(1);
            sttProjectStore.rate = 0.1;
            sttProjectStore.sampleLength = 600;
        }
    },[])

    return (
        <>
            {/* .cell_mid */}
            <div className='cell_mid'>
                <h5>기본설정</h5>
                <div className='lot_item'>
                    <dl>
                        <ComHelpIcon title='Epoch' desc='전체 데이터 셋에 대해 학습할 횟수를 설정합니다.'/>
                        <ComSelect value={sttProjectStore.epochSize} onChange={onEpochSizeChange} options={epochSize} keyName={'epoch'} dispName={'epoch'}/>
                    </dl>
                    <dl>
                        <ComHelpIcon title='Batch Size' desc='한 번에 학습할 음성 개수를 설정합니다.<br/>*보통 1로 설정하고, 전체 음성 길이가 짧으면 더 큰 값으로 설정합니다.'/>
                        <ComSelect value={sttProjectStore.batchSize} onChange={onBatchSizeChange} options={batchSize} keyName={'batch'} dispName={'batch'}/>
                    </dl>
                </div>
                <div className='lot_item'>
                    <dl>
                        <ComHelpIcon title='Validation Rate' desc='성능 검증을 위해 사용할 데이터의 비율입니다.'/>
                        <ComInput ddClass={''} type={'number'} className={sttProjectStore.currentStep === 1 ? 'ipt_txt_enable percent' : 'ipt_txt percent'} value={sttProjectStore.rate} onChange={onValidationRateChange} afterText={'%'} defaultTxt={''} disabled={false}/>
                    </dl>
                    <dl>
                        <ComHelpIcon title='음성길이 설정' desc='학습에 사용할 최대 음성 길이입니다.'/>
                        <ComInput ddClass={''} type={'number'} className={sttProjectStore.currentStep === 1 ? 'ipt_txt_enable percent' : 'ipt_txt percent'} value={sttProjectStore.sampleLength} onChange={onSampleLengthInputChange} afterText={'초'} defaultTxt={''} disabled={false}/>
                    </dl>
                </div>
                <h5>노이즈설정</h5>
                <div className='lot_item'>
                    <dl>
                        <ComHelpIcon title='Noise Data Set' desc='학습에 사용할 노이즈 데이터를 선택합니다.'/>
                        <NoiseDataTable stepId={id}/>
                        {/*<NoiseDataTable data={legacyDataStore.getAvailNoiseData}/>*/}
                    </dl>
                </div>
                <Step2ToggleBox /> {/* 상세설정*/}
                <h5>사전학습모델</h5>

                <div className='lot_item'>
                    <dl>
                        <ComHelpIcon title='Pre Trained Model' desc='사전학습모델을 선택합니다.'/>
                        <Step2PreTrainedModelTreeView/>
                    </dl>
                </div>
            </div>
            {/* //.cell_mid */}

            {/* .cell_btm */}
            <Step2NextBtn title={'설정 저장'} onClick={saveTrainingData}/>
            {/* //.cell_btm */}

            <ComPopBox show={modelRegPopShow}>
                <PopSnapShot stepId={id} close={closePopup} setPopShow={setModelRegPopShow}/>
            </ComPopBox>

        </>
    );
}

export default observer(Step2);
