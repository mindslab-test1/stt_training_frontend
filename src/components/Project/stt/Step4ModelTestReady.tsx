import React, {useContext, useEffect, useState} from 'react';
import Step4UploadBtn from './Step4UploadBtn';
import {observer} from 'mobx-react-lite';
import RootStore from "../../../store/RootStore";
import Step4ModelTestDataTable from "./Step4ModelTestDataTable";

interface Props {
    disb:boolean;
    targetDisb:boolean;
}

const Step4ModelTestReady : React.FC<Props> = ({disb, targetDisb}) => {
    const {sttProjectStore} = useContext(RootStore);
    const LIST_COLUMNS = [
        { title: 'FILE NAME', dataIndex: 'fileName'},
        { title: '음성길이', dataIndex: 'duration'},
        { title: '인코딩', dataIndex: 'encoding'},
    ];

    useEffect(()=>{

    },[]);

    return (
        <>
            <div className='testStep step01'>
                <Step4ModelTestDataTable  title={'업로드 할 데이터 목록'} columns={LIST_COLUMNS} data={sttProjectStore.testingDataList} disb={disb}/>
                <div className='btnBox'>
                    <Step4UploadBtn disb={disb} targetDisb={targetDisb} testReady={sttProjectStore.testingDataList.length === 0}/>
                </div>
                <ul className='lst_info'>
                    <li>*지원가능한 파일 확장자 : .wav & .txt</li>
                    <li>*5MB 이하의 음성 파일을 이용해 주세요.</li>
                </ul>
            </div>
        </>
    );
}

export default observer(Step4ModelTestReady);
