import React, {useContext} from 'react';
import RootStore from '../../../store/RootStore';
import NoiseData from './Step2NoiseData';
import {observer} from 'mobx-react-lite';
import {defaultDisplay, smplRateDisplay} from '../../../utils/CommonUtils';

interface Props{
    stepId:number;
}

const LIST_COLUMNS = [
    { title: 'Name', dataIndex: 'atchFileDisp', displayFtn: defaultDisplay },
    { title: '샘플링레이트', dataIndex: 'smplRate', displayFtn: smplRateDisplay },
    { title: '전체음성길이', dataIndex: 'playSec', displayFtn: defaultDisplay },
    { title: '평균음성길이', dataIndex: 'playSecAvg', displayFtn: defaultDisplay },
];


const Step2NoiseDataTable : React.FC<Props> = ({stepId}) => {
    // console.log('in Step2 TableBox');
    const {sttProjectStore, legacyDataStore} = useContext(RootStore);

    const onClick = (id) => {
        console.log('ComNoiseDataTable onClick:' + id);
        let item = sttProjectStore.noiseDataList.find(obj => {
            if(obj.atchFileId === id){
                sttProjectStore.takeNoiseData(obj);
                return true;
            }
        })
        if(item === undefined){
            item = legacyDataStore.noiseData.find(obj => obj.atchFileId === id);
            sttProjectStore.putNoiseData(item);
        }
    }

    return (
        <NoiseData columns={LIST_COLUMNS} data={legacyDataStore.getAvailNoiseData} onClick={onClick} stepId={stepId}/>
    );
}
export default observer(Step2NoiseDataTable);
