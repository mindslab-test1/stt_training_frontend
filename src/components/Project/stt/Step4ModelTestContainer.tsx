import React, {useContext} from 'react';
import RootStore from '../../../store/RootStore';
import {observer} from 'mobx-react-lite';

import Step4ModelTestReady from './Step4ModelTestReady';
import Step4ModelTestRecord from './Step4ModelTestRecord';
import Step4ModelTestWaiting from './Step4ModelTestWaiting';
import Step4ModelTestProcessing from './Step4ModelTestProcessing';
import Step4ModelTestDone from './Step4ModelTestDone';
import Step4ModelTestError from './Step4ModelTestError';

interface Props {
  disb:boolean;
  targetDisb:boolean;
}

const ModelTestContainer : React.FC<Props> = ({disb, targetDisb}) => {
  const {modelTestStore} = useContext(RootStore);
  let stepComponent;
  switch (modelTestStore.stepNum){
    case 1 :
      stepComponent = <Step4ModelTestReady disb={disb} targetDisb={targetDisb}/>
      break;
    case 2 :
      stepComponent = <Step4ModelTestRecord />
      break;
    case 3 :
      stepComponent = <Step4ModelTestWaiting />
      break;
    case 4 :
      stepComponent = <Step4ModelTestProcessing />
      break;
    case 5 :
      stepComponent = <Step4ModelTestDone />
      break;
    case 6 :
      stepComponent = <Step4ModelTestError />
      break;
  }
  return (
    <>
      {stepComponent}
    </>
  );
}

export default observer(ModelTestContainer);
