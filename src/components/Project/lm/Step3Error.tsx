import React, {useContext} from 'react';
import errorGif from 'assets/images/ani_error.gif';
import RootStore from '../../../store/RootStore';

interface Props {
}

const Step3Error : React.FC<Props> = ({}) => {

    const {lmProjectStore} = useContext(RootStore);

    const backToStep2 = () => {
        void lmProjectStore.updateStep(lmProjectStore.id, lmProjectStore.getStep()-1);
    }

    return (
        <div className='fifo_error'>
            <div className='aniBox'><img src={errorGif} alt='학습 에러'/></div>
            <p className='tit'>죄송합니다. <br/>학습 중 에러가 발생하였습니다.</p>
            <p className='txt'>현재 문제를 해결하기 위해 담당부서에서 확인 중에 있습니다.<br/>서비스 이용에 불편을 드려 대단히 죄송합니다.</p>
            <p className='txt_s'/>
            <button type='button' className='btn_learning_cancel' onClick={backToStep2}>이전 단계로 돌아가기</button>
            {/*<button type='button' className='btn_learning_cancel' onClick={()=>trainingStore.trainingCancel('Step3Error')}>이전 단계로 돌아가기</button>*/}
        </div>
    );
}

export default Step3Error;
