import React, {useContext} from 'react';
import RootStore from '../../../store/RootStore';
import fifoGif from 'assets/images/ani_fifo.gif';

interface Props {
}

const Step3Waiting : React.FC<Props> = () => {

    const { trainingStore} = useContext(RootStore);


/*    const onClickCancel = async () => {

        console.log(trainingStore.getSnapshotId(),  trainingStore.getModelId());

        let param = {id : trainingStore.getSnapshotId(), modelId: trainingStore.getModelId()};
        const _api = deepCopyObject(API.TRAINING_CANCEL);

        await callApi(_api, param).then((res)=>{
            if(res.status === 200){
                console.dir(res.data);
                lmProjectStore.completedStep = lmProjectStore.getStep()-1;
                lmProjectStore.setStep(lmProjectStore.getStep()-1);
                notiStore.infoGreen('학습 취소 성공');
            }else{
                notiStore.error('학습 취소 실패');
                console.log('%c HTTP status : ' + res.status + ' : cancel training fail\n', res.data);
            }
        }).catch((res)=>{
            notiStore.error('학습 취소 실패');
            console.log('%c HTTP status : ' + res.status + ' : cancel training fail\n', res.data);
        })
    }*/

    return (
        <div className='fifo'>
            <div className='aniBox'><img src={fifoGif} alt='학습 대기중'/></div>
            <p className='tit'>잠시만 기다려 주세요.
            {
                trainingStore.getOrder() > 0 ?
                    <><br/>현재 <span className='ft_point_orange'>
                    {trainingStore.getOrder()}</span>개의 <span className='ft_point_orange'>학습이 대기 중</span>입니다.</>
                    :
                    <><br/>현재 <span className='ft_point_orange'>학습을 위한 환경을 구성하고</span> 있습니다.</>
            }
            </p>
            <p className='txt'>안정적인 학습 환경을 제공하기 위해서<br/>학습을 제한하고 있습니다.</p>
            <p className='txt_s'>학습 취소 버튼을 클릭 하실 경우, <br/>대기 순위가 초기화 될 수 있습니다.</p>
            <button type='button' className='btn_learning_cancel' onClick={trainingStore.trainingCancel}>학습 취소</button>
        </div>
    );
}

export default Step3Waiting;
