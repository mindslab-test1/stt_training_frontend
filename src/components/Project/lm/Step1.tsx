import React, {useContext, useState} from 'react';

import RootStore from '../../../store/RootStore';
import ComHelpIcon from '../../commons/ComHelpIcon';
import ComSelect from '../../commons/ComSelect';
import Step1AvailableLearningDataTable from './Step1AvailableLearningDataTable';
import Step1SelectedLearningDataTable from './Step1SelectedLearningDataTable';
import Step1NextBtn from './Step1NextBtn';
import {observer} from 'mobx-react-lite';
import PopSnapShot from "../../popup/PopSnapShot";
import ComPopBox from "../../commons/ComPopBox";

interface Props {
  id : number;
}

const Step1 : React.FC<Props> = ({id}) => {
  // console.log('%c In Step1', 'color:red');
  const {legacyDataStore, notiStore, lmProjectStore} = useContext(RootStore);
  const [params, setParams] = useState(['']);
  const [modelRegPopShow, setModelRegPopShow] = useState(false);

  const saveLearningData = e => {
    if(lmProjectStore.learningDataList.length < 1) {
      notiStore.warning('학습 데이터를 1개 이상 선택해 주세요.');
      return false;
    }
    void lmProjectStore.saveLearningData(id, setModelRegPopShow, modelRegPopShow); // learningData 저장 및 성공시 setStep
  }

  const EOS_DATA = [
    // {key: null, value : '전체'},
    {key: 'Y', value : 'Y'},
    {key: 'N', value : 'N'},
  ];

  /*FILE UPLOAD 방식으로의 변경으로 인해 onSearch함수 주석처리*/

  const onLanguageChange = (e) => {
    lmProjectStore.setLanguage(e.target.value);
  }

  const closePopup = () => {
    setModelRegPopShow(false);
  }

  return (
    <>
      {/* .cell_mid */}
      <div className='cell_mid'>
        <div className='lot_item'>
          <dl>
            <ComHelpIcon title='언어' desc='조회할 데이터 언어 형태를 선택합니다.'/>
            <ComSelect value={lmProjectStore.language} onChange={onLanguageChange} options={legacyDataStore.getLang()} keyName={'code'} dispName={'codeDisp'}/>
          </dl>
        </div>

        <div className='lot_item sum_hide'>
          <dl>
            <ComHelpIcon title='업로드 할 데이터' desc='업로드 할 데이터 목록입니다.'/>
            <Step1AvailableLearningDataTable/>
          </dl>
        </div>
        <div className='btnSqBox'>
          <button type='button' onClick={() => void lmProjectStore.uploadData(1, undefined)}>업로드</button>
          <button type='button' onClick={() => legacyDataStore.setLearnData([])}>초기화</button>
        </div>
        <div className='lot_item'>
          <dl>
            <ComHelpIcon title='학습에 사용할 데이터' desc='학습에 사용할 데이터 입니다.'/>
            <Step1SelectedLearningDataTable stepId={id}/>
          </dl>
        </div>
      </div>
      {/* //.cell_mid */}

      {/* .cell_btm */}
      <Step1NextBtn title={'설정 저장'} onClick={saveLearningData}/>
      {/* //.cell_btm */}
      <ComPopBox show={modelRegPopShow}>
        <PopSnapShot stepId={id} close={closePopup} setPopShow={setModelRegPopShow}/>
      </ComPopBox>
    </>
  );
}

export default observer(Step1);
