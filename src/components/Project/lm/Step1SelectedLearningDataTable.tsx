import React, {useContext} from 'react';
import SelectedLearningData from './Step1SelectedLearningData';
import RootStore from '../../../store/RootStore';
import {observer} from 'mobx-react-lite';
import {defaultDisplay, smplRateDisplay} from '../../../utils/CommonUtils';

interface Props {
  stepId:number;
}

const LIST_COLUMNS = [
  { title: 'Name', dataIndex: 'atchFilePairName', displayFtn: defaultDisplay },
];

const Step1SelectedLearningDataTable: React.FC<Props> = ({stepId}) => {
  // console.log('in ComSelectedLearningDataTable');
  const {lmProjectStore, legacyDataStore, notiStore} = useContext(RootStore);

  const onClick = (id) => {
    console.log('ComSelectedLearningDataTable onClick:' + id);
    if(confirm('삭제 하시겠습니까?')){
      void lmProjectStore.deleteData(id);
    }
    /*const item = lmProjectStore.learningDataList.find(it => it.atchFileId === id);
    lmProjectStore.takeLearningData(item);*/
  }

  return (
      <SelectedLearningData title={'학습에 사용할 데이터 목록'} columns={LIST_COLUMNS} data={lmProjectStore.learningDataList} onClick={onClick} stepId={stepId}/>
  );
};

export default observer(Step1SelectedLearningDataTable);
