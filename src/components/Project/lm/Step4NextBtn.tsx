import React, {useContext, useState} from 'react';
import RootStore from '../../../store/RootStore';
import {observer} from 'mobx-react-lite';
import ComNextBtn from '../../commons/ComNextBtn';
import PopContact from '../../popup/PopContact';
import ComPopBox from '../../commons/ComPopBox';

interface Props {
    title: string;
}

const Step4NextBtn: React.FC<Props> = ({title}) => {

    const {modelTestStore, lmProjectStore} = useContext(RootStore);
    // const btnState = !(modelTestStore.currentModel && modelTestStore.currentModel.length !== 0);
    const btnState = false;

    const [popShow, setPopShow] = useState(false);

    const setPopIsOpen = () => setPopShow(!popShow);
    const closePopup = () => setPopShow(false);


    /*const onClickCreateAPI = () => {
    }

    return ( <ComNextBtn title={title} onClick={onClickCreateAPI} disabled={btnState}/> );*/


    return(
        <>
            <ComNextBtn title={title} onClick={setPopIsOpen} disabled={btnState}/>
            <ComPopBox show={popShow}>
                <PopContact close={closePopup} popTitle={'학습 모델 문의하기'} modelId={true} modelName={lmProjectStore.modelName}/>
            </ComPopBox>
        </>
    )
};

export default observer(Step4NextBtn);
