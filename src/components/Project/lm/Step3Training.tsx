import React, {useContext, useState} from 'react';
import RootStore from '../../../store/RootStore';
import {observer} from 'mobx-react-lite';
import SockJsClient from 'react-stomp';

interface Props {
}

const Step3Training : React.FC<Props> = () => {

  const {trainingStore, lmProjectStore} = useContext(RootStore);
  const [logMessage, setLogMessage] = useState(new Array());
  const logData = trainingStore.dataPoints;

  return (
      <div className='fifo_none'>
        <div className='lot_item'>
          <dl>
            <dt>학습모델 아이디
              <span className='ico_help'><em>학습모델의 고유코드입니다.</em></span>
            </dt>
            <dd>{trainingStore.modelId}</dd>
            {/* [D] STT학습모델 아이디 규칙: 사용자ID_학습데이터Type_생성일시분_엔진type_샘플링레이트 userId_voice_20200916130221_STT_kor_16k*/}
          </dl>
        </div>
        <div className='lot_item'>
          <dl>
            <dt>학습로그
              <span className='ico_help'><em>학습이 수행되면서 출력된 로그입니다.</em></span>
            </dt>
            <dd className='chartBox' style={{maxHeight: "700px",whiteSpace: "pre-wrap", overflowY: "auto"}}>
                {logData.flat ? logData.flat : logData}
                {
                    logMessage.map((message, index) => <p key={index}>{message}</p>)
                }
            </dd>
          </dl>
        </div>
          <SockJsClient url='/ws'
                        topics={['/ws/subscribe/training/lm/'+lmProjectStore.id+'/'+trainingStore.snapshotId]}
                        onMessage={(msg) => {
                            setLogMessage(logMessage.concat([msg]));
                        }}
          />
      </div>
  );
}

export default observer(Step3Training)

;
