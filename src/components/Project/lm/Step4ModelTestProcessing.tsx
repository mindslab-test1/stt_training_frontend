import React, {useContext, useEffect} from 'react';
import RootStore from '../../../store/RootStore';

interface Props {

}

const Step4ModelTestProcessing : React.FC<Props> = () => {
    const {modelTestStore} = useContext(RootStore);
    let interval;

    useEffect(()=>{
        interval = setInterval( async () => {
            modelTestStore.fetchSttTestStatus().then(r => console.log('Running: 상태 조회'))
        },10000);
        return () => {
            clearInterval(interval);
        }
    },[]);

    return (
        <>
            {/*step04(음성변환 화면)*/}
            <div className='testStep step04'>
                <div className='setpBox'>
                    <div className='ani_mix'>
                        <span className='mixDot01' />
                        <span className='mixDot02' />
                    </div>
                    <p className='ani_mesg mgb30'>음성을 변환 중입니다.<br />잠시만 기다려 주세요.</p>
                </div>
            </div>
            {/*//step04*/}
        </>
    );
}

export default Step4ModelTestProcessing;
