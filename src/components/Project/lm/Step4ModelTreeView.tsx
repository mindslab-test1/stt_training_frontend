import React, {useContext} from 'react';
import RootStore from '../../../store/RootStore';
import ComTreeView from '../../commons/ComTreeView';
import {observer} from 'mobx-react-lite';

interface Props {
  disb:any;
}

const Step4ModelTreeView : React.FC<Props> = ({disb}) => {
  const {modelTestStore} = useContext(RootStore);
  const testModel = modelTestStore.getTestModelList;
  const selectedFileName = (modelTestStore.currentModel && modelTestStore.currentModel.modelName)
    ? modelTestStore.currentModel.modelId : '';
  const selectedModelName = (modelTestStore.currentModel && modelTestStore.currentModel.modelId)
    ? modelTestStore.currentModel.modelId : '';

  const onClick = (name, id, sampleRate) => {
    // console.log(name);
    modelTestStore.setCurrentModel(name, id);
    disb(false);
  }

  return (
    <dd className='treeBox scroll_s' id='treeSelect'>
      <ul className='filetree'>
        <ComTreeView data={testModel} selectedMember={selectedFileName} selectedGroup={selectedModelName} onClick={onClick} />
      </ul>
    </dd>
  );
};

export default observer(Step4ModelTreeView);
