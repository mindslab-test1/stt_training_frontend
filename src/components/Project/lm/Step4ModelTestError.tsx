import React, {useContext} from 'react';
import RootStore from '../../../store/RootStore';

interface Props {

}

const Step4ModelTestError : React.FC<Props> = () => {
    const {modelTestStore} = useContext(RootStore);
    const recordBtn = () => {
        modelTestStore.setStepNum(1);
    }
    return (
        <>
            {/*step03(에러화면) */}
            <div className='testStep error'>
                <div className='setpBox'>
                    <p className='ani_mesg'><strong>죄송합니다.<br />테스트 진행 중 오류가 발생하였습니다.</strong>현재 문제를 해결하기 위해 담당부서에서 확인 중에 있습니다.<br />서비스 이용에 불편을 드려 대단히 죄송합니다.</p>
                    <div className='btnBox'>
                        <button type='button' className='ico_reset btn_clr' onClick={recordBtn}>처음으로</button>
                    </div>
                </div>
            </div>
            {/*step03(에러화면)*/}
        </>
    );
}

export default Step4ModelTestError;
