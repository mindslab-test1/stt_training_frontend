import React, {useContext} from 'react';
import RootStore from '../../../store/RootStore';

interface Props {
    disb:boolean;
}

const Step4RecordBtn : React.FC<Props> = ({disb}) => {
    const {modelTestStore} = useContext(RootStore);

    let style;
    if(disb) style='disabled';


    const recordBtn = () => {
        const constraints = {
            audio: true
        }
        navigator.mediaDevices.getUserMedia(constraints)
            .then(stream => {
                modelTestStore.setStepNum(2);

            })
            .catch(err => {
                alert('마이크를 허용해 주세요.');
                console.log('The following error occurred: ' + err);
            })

    }

    return (
        <>
            <button type='button' className='btn_clr ico_record' onClick={recordBtn} disabled={style}>녹음</button>
        </>
    );
}

export default Step4RecordBtn;