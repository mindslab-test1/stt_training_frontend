import React, {useContext, useEffect, useState} from 'react';
import ComPopBox from '../../commons/ComPopBox';
import PopHistoryList from '../../popup/PopHistoryList';
import {deepCopyObject} from '../../../utils/CommonUtils';
import {API, callApi} from '../../../service/ApiService';
import RootStore from '../../../store/RootStore';
import Step4ModelTreeView from './Step4ModelTreeView';
import {observer} from 'mobx-react-lite';
import Step4ModelTestContainer from './Step4ModelTestContainer';
import Step4NextBtn from './Step4NextBtn';
import Step4ModelTestReady from "./Step4ModelTestReady";
import Step4ModelTestLog from "./Step4ModelTestLog";
import Step4TargetModelTreeView from "../stt/Step4TargetModelTreeView";

interface Props {
    id : number;
}

const Step4 : React.FC<Props> = ({id}) => {
    const {lmProjectStore, modelTestStore} = useContext(RootStore);
    const [historyList, setHistoryList] = useState([]);
    const [popShow, setPopShow] = useState(false);
    const [stepNum, setStepNum] = useState('1')
    const [disb, setDisb] = useState(true);
    const [targetDisb, setTargetDisb] = useState(true);

    const getSnapShot = async(id) => {
        try{
            const _api = deepCopyObject(API.PROJECT_SNAPSHOT_LIST);
            _api.url = _api.url.replace('_ID_', id);
            const res = await callApi(_api);
            console.dir(res.data);
            setHistoryList(res.data);
            setPopShow(!popShow);

        }catch (err){
            console.error(err);
        }
    }

    const closePopup = () => {
        setPopShow(false);
    }
    modelTestStore.setStepNum(1);
    let fileActive = document.getElementsByClassName('file');
    useEffect( () => {
        for(let i=0; i <fileActive.length; i ++){
            fileActive[i].classList.remove('active')
        }
    },[]);

    const modelName = () => {
        if(modelTestStore.currentModel){
            return <label style={{marginLeft: '20px'}}>{modelTestStore.currentModel.modelPath + '/' + modelTestStore.currentModel.modelFileName}</label>
        }
    }

    const targetModelName = () => {
        if(modelTestStore.currentTargetModel){
            return <label style={{marginLeft: '20px'}}>{modelTestStore.currentTargetModel.modelPath + '/' + modelTestStore.currentTargetModel.modelFileName}</label>
        }
    }


    return (
        <>
            {/* .cell_mid */}
            <div className='cell_mid'>
                <div className='lot_item'>
                    <dl>
                        <dt>학습모델
                            <span className='ico_help'><em>학습이 완료된 모델과 데이터입니다.</em></span>
                            {modelName()}
                        </dt>
                        <Step4ModelTreeView disb={setDisb}/>
                    </dl>
                </div>
                <div className='lot_item'>
                    <dl>
                        <dt>테스트 AM 모델
                            <span className='ico_help'><em>LM 학습이 완료된 모델과 데이터입니다.</em></span>
                            {targetModelName()}
                        </dt>
                        <Step4TargetModelTreeView disb={setTargetDisb}/>
                    </dl>
                </div>
                {/*lot_item */}
                <div className='lot_item'>
                    <dl>
                        <dt>테스트
                            <span className='ico_help'><em>학습이 완료된 데이터를 선택하여 간단한 테스트를 진행할 수 있습니다.</em></span>
                        </dt>
                        <div className='enTestBox'>
                            <Step4ModelTestContainer disb={disb} targetDisb={targetDisb}/>
                        </div>
                    </dl>
                </div>

                <div className='lot_item'>
                    <dl>
                        <dt>테스트 결과
                            <span className='ico_help'><em>테스트 결과가 아래 textarea에 표시됩니다.</em></span>
                        </dt>
                        <dd className='enTestBox'>
                            {
                                lmProjectStore.testState ?
                                    <Step4ModelTestLog/> :
                                    <>평가를 진행해주세요.</>
                            }
                        </dd>
                    </dl>
                </div>
            </div>
            {/* //.cell_mid */}

            {/* .cell_btm */}
            {/*<div className='cell_btm'>
                <div className='btnSqBox'>
                    <a id='btn_apiAdd' onClick={lmProjectStore.downloadModel} className='btn_lyr_open'>다운로드</a>
                    <a id='btn_apiAdd' className='btn_lyr_open'>문의하기</a>
                </div>
            </div>*/}
            {/* //.cell_btm */}

            <ComPopBox show={popShow}>
                <PopHistoryList close={closePopup} historyList={historyList} />
            </ComPopBox>
        </>
    );
}

export default observer(Step4);
