import { Cookies } from 'react-cookie';
import { callApi, API, HTTP_RES_CODE } from 'service/ApiService';
import { ERouteUrl } from 'router/RouteLinks';
import { IContainerBaseProps } from 'constants/BaseInterface';
import jwt_decode from 'jwt-decode';
import {deepCopyObject} from '../utils/CommonUtils';

export const signup = async (form: any, navigation: IContainerBaseProps, handleError: Function, handleSetProfile: Function) => {
  const res = await callApi(API.SIGNUP, form);

  if (HTTP_RES_CODE.CREATED === res?.status) {
    const loginForm = {
      id: form.email,
      password: form.token,
    };
    void login(loginForm, navigation, handleError, handleSetProfile);
  }
};

export const login = async (form: object, navigation: IContainerBaseProps, handleError: Function, handleSetProfile: Function) => {
  const res = await callApi(API.LOGIN, form);

  if (HTTP_RES_CODE.OK === res?.status) {
    storeUserToken(res.data.accessToken, handleSetProfile);
    navigation.history.push(ERouteUrl.HOME);
  } else {
    handleError();
  }

};export const loginCallback = async (navigation: IContainerBaseProps,param:string, handleError: Function, handleSetProfile: Function) => {
  const callbackApi = deepCopyObject(API.LOGIN_CHECK);
  callbackApi.url = callbackApi.url + param;
  const res = await callApi(callbackApi);
  if (HTTP_RES_CODE.OK === res?.status) {
    storeUserToken(res.data.accessToken, handleSetProfile);
    navigation.history.push(ERouteUrl.HOME);
  } else if (HTTP_RES_CODE.NOT_FOUND === res?.status) {
    alert(res?.data.message);
    navigation.history.push(ERouteUrl.SSO_LOGIN);
  } else {
    handleError();
  }
};

export const storeUserToken = (token: string, handleSetProfile: Function) => {
  // const d = new Date();
  // d.setTime(d.getTime() + 60 * 60 * 24 * 7);
  const maxAge = 60 * 60 * 24 * 365;
  const cookies = new Cookies();
  const decoded = jwt_decode(token);

  cookies.set('userToken', token, { path: '/', maxAge });
  handleSetProfile(decoded.name, decoded.email);
};

export const removeUserToken = () => {
  const cookies = new Cookies();
  cookies.remove('userToken', { path: '/' });
  window.location.href = process.env.REACT_APP_SSO_LOGOUT_URL + '?access_token=1&client_id='
    + process.env.REACT_APP_SSO_CLIENT_ID + '&returnUrl=' + process.env.REACT_APP_SSO_LOGOUT_RETURN_URL;
};

export const pullUserToken = () => {
  const cookies = new Cookies();
  return cookies.get('userToken');
};

export const isLoggedIn = async () => {
  return new Promise(async (resolve) => {
    const res = await callApi(API.AUTH_PING);
    resolve(HTTP_RES_CODE.OK === res?.status);
  });
};
