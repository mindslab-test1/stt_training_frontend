import axios, { AxiosResponse } from 'axios';
import { replacePage } from '../utils/Navigation';
import { ERouteUrl } from 'router/RouteLinks';
import { pullUserToken } from 'service/AuthService';

enum HTTP_METHOD {
  GET = 'GET',
  POST = 'POST',
  PATCH = 'PATCH',
  PUT = 'PUT',
  DELETE = 'DELETE',
  DOWNLOAD = 'DOWNLOAD',
}

export enum HTTP_RES_CODE {
  OK = 200,
  CREATED = 201,
  ACCEPTED = 202,
  NO_CONTENT = 204,
  BAD_REQUEST = 400,
  NOT_FOUND = 404,
  UNAUTHORIZED = 401,
  SERVER_ERROR = 500,
}

export interface IApiDefinition {
  url: string;
  method: HTTP_METHOD;
  successCode: HTTP_RES_CODE;
}

export interface IApiResponse {
  status: HTTP_RES_CODE;
  message?: string;
  data?: any;
  success: boolean;
  unauthorized: boolean;
}

const BASE_URL = process.env.REACT_APP_API_URL;

export const API = {
  SIGNUP: { url: '/auth/admin/signup', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.CREATED },
  LOGIN: { url: '/auth/user/login', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  LOGIN_CHECK: { url: '/auth/user/callback', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  AUTH_PING: { url: '/auth/user/ping', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },

  INQUIRY_REG: { url: '/inquiry', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },

  PROJECT_LIST: { url: '/project/', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  PROJECT_INFO: { url: '/project/_ID_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  PROJECT_DELETE: { url: '/project/_ID_', method: HTTP_METHOD.DELETE, successCode: HTTP_RES_CODE.OK },
  PROJECT_FILE_DELETE: { url: '/project/file/_ID_', method: HTTP_METHOD.DELETE, successCode: HTTP_RES_CODE.OK },
  PROJECT_SNAPSHOT_LIST: { url: '/project/_ID_/snapshot', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  PROJECT_SNAPSHOT_REG: { url: '/project/_ID_/snapshot', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  PROJECT_SNAPSHOT_APPLY: { url: '/project/_ID_/snapshot/_SNAPSHOT_ID_', method: HTTP_METHOD.PATCH, successCode: HTTP_RES_CODE.OK },
  PROJECT_MODEL_DATA_LIST: { url: '/project/model', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  PROJECT_TEST_MODEL_DATA_LIST: { url: '/project/_ID_/model', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  PROJECT_TEST_TARGET_MODEL_DATA_LIST: { url: '/project/_ENGN_/_LANG_/model', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },

  PROJECT_STT_REG: { url: '/project/stt', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  PROJECT_STT_DETAIL: { url: '/project/stt/_ID_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  PROJECT_STT_UPDATE_STEP: { url: '/project/stt/_ID_/_STEP_', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  PROJECT_STT_REG_LEARNING_DATA: { url: '/project/stt/_ID_/learningData', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  PROJECT_STT_REG_LEARNING_DATA_FILE: { url: '/project/stt/_ID_/learningData/file', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  PROJECT_STT_LEARNING_DATA_UPLOAD: { url: '/project/_ID_/snapshot/_SNAPSHOT_ID_', method: HTTP_METHOD.PATCH, successCode: HTTP_RES_CODE.OK },
  PROJECT_STT_REG_TRAINING_DATA: { url: '/project/stt/_ID_/trainingData', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  PROJECT_STT_NOISE_DATA_LIST: { url: '/project/stt/noise', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },

  PROJECT_LM_REG: { url: '/project/lm', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  PROJECT_LM_DETAIL: { url: '/project/lm/_ID_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  PROJECT_LM_UPDATE_STEP: { url: '/project/lm/_ID_/_STEP_', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  PROJECT_LM_REG_LEARNING_DATA: { url: '/project/lm/_ID_/learningData', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  PROJECT_LM_REG_LEARNING_DATA_FILE: { url: '/project/lm/_ID_/learningData/file', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  PROJECT_LM_LEARNING_DATA_UPLOAD: { url: '/project/_ID_/snapshot/_SNAPSHOT_ID_', method: HTTP_METHOD.PATCH, successCode: HTTP_RES_CODE.OK },
  PROJECT_LM_REG_TRAINING_DATA: { url: '/project/lm/_ID_/trainingData', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  PROJECT_LM_NOISE_DATA_LIST: { url: '/project/lm/noise', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },

  // code api
  CODE_GROUP: { url: '/code/_CODE-GROUP_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },

  // mock api
  MOCK_LANG: { url: '/lang', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  MOCK_SAMPLERATE: { url: '/samplerate', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  MOCK_MODELDATA: { url: '/model', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  MOCK_NOISEDATA: { url: '/noiseData', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  MOCK_TEST_MODEL_DATA: { url: '/testModelData', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },

  // data api
  DMNG_ENGINE_LIST: { url: '/dmng/getEngineList', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DMNG_LANG_LIST: { url: '/dmng/getLang', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DMNG_LEARNING_DATA_LIST: { url: '/dmng/getLearningDataList', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DMNG_NOISE_DATA_LISE: { url: '/dmng/getNoiseDataList', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DMNG_SAMPLE_RATE_LIST: { url: '/dmng/getSampleRateList', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DMNG_PRE_TRAINED_MODEL_COMMON_LIST: { url: '/dmng/getCommonModelList', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DMNG_PRE_TRAINED_MODEL_PRIVATE_LIST: { url: '/dmng/getMyModelList', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },
  DMNG_MODELFILE_LIST: { url: '/dmng/getModelFileList/_ID_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK },

  // Training api
  TRAINING_BEGIN: { url: '/training/begin', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK},
  TRAINING_CANCEL: { url: '/training/cancel', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK},
  TRAINING_FINISH: { url: '/training/finish', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK},
  TRAINING_MONITORING: { url: '/training/monitoring/_ID_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK},
  TRAINING_EPOCH_MONITORING: { url: '/training/monitoring/epoch-time/_ID_/_SID_', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK},

  // Model Test Controller
  MODEL_TEST_BEGIN: {url: '/test/begin', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK},
  MODEL_TEST_CANCEL: {url: '/test/cancel', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK},
  MODEL_TEST_STATUS: {url: '/test/status', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK},
  MODEL_TEST_START: { url: '/test/start', method: HTTP_METHOD.POST, successCode: HTTP_RES_CODE.OK },
  MODEL_DOWNLOAD: { url: '/test/download', method: HTTP_METHOD.DOWNLOAD, successCode: HTTP_RES_CODE.OK },

  // Monitoring
  SERVICE_MONITORING: {url: '/monitoring/service', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK},
  DATA_PROCESSING_MONITORING: {url: '/monitoring/data', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK},
  STT_PROCESSING_MONITORING: {url: '/monitoring/stt', method: HTTP_METHOD.GET, successCode: HTTP_RES_CODE.OK}

};

export const callApi = async (apiUrl: IApiDefinition, params?: object) => {
  let headers = {};

  // TODO: Set Token
  const token = pullUserToken();
  if (token) {
    headers['Authorization'] = 'Bearer ' + token;
  }

  const _axios = axios.create({
    baseURL: BASE_URL,
    headers,
  });

  try {
    let res: AxiosResponse<any>;
    if (HTTP_METHOD.DOWNLOAD === apiUrl.method) {
      console.log(params);
      res = await _axios.post(encodeURI(apiUrl.url), params, {
        onDownloadProgress: (progressEvent) => {
          console.log(progressEvent);
        },
        responseType: 'blob'
      });
    } else if (HTTP_METHOD.POST === apiUrl.method) {
      res = await _axios.post(encodeURI(apiUrl.url), params);
    } else if (HTTP_METHOD.PUT === apiUrl.method) {
      res = await _axios.put(encodeURI(apiUrl.url), params);
    } else if (HTTP_METHOD.PATCH === apiUrl.method) {
      res = await _axios.patch(encodeURI(apiUrl.url), params);
    } else if (HTTP_METHOD.DELETE === apiUrl.method) {
      res = await _axios.delete(encodeURI(apiUrl.url), params);
    } else {
      // Get
      const _apiUrl = apiUrl.url + appendGetParams(params);
      res = await _axios.get(encodeURI(_apiUrl));
    }

    return returnResponse({ success: true, unauthorized: false, status: res.status, data: res.data });
  } catch (err) {
    console.log('API ERROR !!!!!!!!!!!!: ', err.response);
    let unauthorized = false;

    if (err.response) {
      // api call success, response != 2XX
      unauthorized = HTTP_RES_CODE.UNAUTHORIZED === err.response.status;
      if (process.env.NODE_ENV !== 'development') {
        window.location.href = '/error';
      }
    }
    else if (err.request) {
      // api call failed, network error.
      if (process.env.NODE_ENV !== 'development') {
        window.location.href = '/network_error';
      }
    }
    else {
      // client side error.
      if (process.env.NODE_ENV !== 'development') {
        window.location.href = '/error';
      }
    }


    // handleUnauthorized(err.response.status);
    return returnResponse({ success: false, unauthorized, status: err.response.status, data: err.response.data });
  }
};

export const callMockApi = async (apiUrl: IApiDefinition, params?: object) => {
  let headers = {};

  // TODO: Set Token
  const token = pullUserToken();
  if (token) {
    headers['Authorization'] = 'Bearer ' + token;
  }

  const _axios = axios.create({
    baseURL: '/mock-api',
    headers,
  });

  try {
    let res: AxiosResponse<any>;
    if (HTTP_METHOD.POST === apiUrl.method) {
      res = await _axios.post(encodeURI(apiUrl.url), params);
    } else if (HTTP_METHOD.PUT === apiUrl.method) {
      res = await _axios.put(encodeURI(apiUrl.url), params);
    } else if (HTTP_METHOD.PATCH === apiUrl.method) {
      res = await _axios.patch(encodeURI(apiUrl.url), params);
    } else if (HTTP_METHOD.DELETE === apiUrl.method) {
      res = await _axios.delete(encodeURI(apiUrl.url), params);
    } else {
      // Get
      const _apiUrl = apiUrl.url + appendGetParams(params);
      res = await _axios.get(encodeURI(_apiUrl));
    }

    return returnResponse({ success: true, unauthorized: false, status: res.status, data: res.data });
  } catch (err) {
    // console.log(err.response);
    let unauthorized = false;

    if (err.response) {
      unauthorized = HTTP_RES_CODE.UNAUTHORIZED === err.response.status;
    }
    // handleUnauthorized(err.response.status);
    return returnResponse({ success: false, unauthorized, status: err.response.status, data: err.response.data });
  }
};

const returnResponse = (params: IApiResponse) => {
  return params;
};

export const appendGetParams = (params) => {
  if (!params) return '';

  let paramSuffix = '';

  let loop = 0;
  for (const paramKey in params) {
    // eslint-disable-next-line no-prototype-builtins
    if (!params.hasOwnProperty(paramKey)) {
      continue;
    }

    const symbol = loop === 0 ? '?' : '&';
    paramSuffix += symbol + paramKey + '=' + params[paramKey];
    loop++;
  }

  return paramSuffix;
};

// const handleUnauthorized = (status: HTTP_RES_CODE) => {
//   if (HTTP_RES_CODE.UNAUTHORIZED !== status) return false;
//   replacePage(ERouteUrl.LOGIN);
//   // if ( navigation !== undefined ) {
//   //   logout( navigation );
//   // }
//   return true;
// };


// 모델 테스트 API 용으로, 다른 API와 헤더값이 다름
export const callTestApi = async (apiUrl: IApiDefinition, params?: object) => {
  let headers ={'Content-Type': 'multipart/form-data'};

  // TODO: Set Token
  const token = pullUserToken();
  if (token) {
    headers['Authorization'] = 'Bearer ' + token;
  }

  const _axios = axios.create({
    baseURL: BASE_URL,
    headers,
  });

  try {
    let res: AxiosResponse<any>;
    if (HTTP_METHOD.POST === apiUrl.method) {
      res = await _axios.post(encodeURI(apiUrl.url), params);
    } else if (HTTP_METHOD.PUT === apiUrl.method) {
      res = await _axios.put(encodeURI(apiUrl.url), params);
    } else if (HTTP_METHOD.PATCH === apiUrl.method) {
      res = await _axios.patch(encodeURI(apiUrl.url), params);
    } else if (HTTP_METHOD.DELETE === apiUrl.method) {
      res = await _axios.delete(encodeURI(apiUrl.url), params);
    } else {
      // Get
      const _apiUrl = apiUrl.url + appendGetParams(params);
      res = await _axios.get(encodeURI(_apiUrl));
    }

    return returnResponse({ success: true, unauthorized: false, status: res.status, data: res.data });
  } catch (err) {

    console.log('API ERROR !!!!!!!!!!!!: ' + err.response);
    let unauthorized = false;

    if (err.response) {
      // api call success, response != 2XX
      unauthorized = HTTP_RES_CODE.UNAUTHORIZED === err.response.status;
      if(unauthorized){
        window.location.href = process.env.REACT_APP_SSO_LOGIN_URL + '?response_type=code&client_id='
            + process.env.REACT_APP_SSO_CLIENT_ID + '&redirect_uri=' + process.env.REACT_APP_CALLBACK_URL;
      }else{
        /*if (process.env.NODE_ENV !== 'development') {
          window.location.href = '/error';
        }*/
      }
    }
    else if (err.request) {
      // api call failed, network error.
      if (process.env.NODE_ENV !== 'development') {
        window.location.href = '/network_error';
      }
    }
    else {
      // client side error.
      /*if (process.env.NODE_ENV !== 'development') {
        window.location.href = '/error';
      }*/
    }


    // handleUnauthorized(err.response.status);
    return returnResponse({ success: false, unauthorized, status: err.response.status, data: err.response.data });
  }
};
