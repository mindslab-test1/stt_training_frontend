import React, {ChangeEvent, useCallback, useContext, useEffect, useReducer, useState} from 'react';
import {ComPage} from '../../components/commons';
import RootStore from "../../store/RootStore";
import {deepCopyObject} from "../../utils/CommonUtils";
import {API, callApi} from "../../service/ApiService";
import moment from "moment";
import DatePicker from "react-datepicker";
import ko from "date-fns/esm/locale";
import "react-datepicker/dist/react-datepicker.css"

interface Props{}

interface DataProcessingMonitoringResponse {
    processingDays: number;
    avgTimePerDay: string;
    requestTime: string;
    processingTime: string;
}

interface SttProcessingMonitoringResponse {
    sttRequestCount: number;
    sttWaitingCount: number;
    sttProgressCount: number;
    sttCompletedCount: number;
    sttErrorCount: number;
}

interface SearchParam {
    startDate: string;
    endDate: string;
}

interface SttSearchParam {
    date: string;
    startTime?: string;
    endTime?: string;
}

const ConDataProcessingMonitoring: React.FC<Props> = () => {

    const date = new Date();
    const {notiStore} = useContext(RootStore);
    const [dataProcessingMonitoringData, setDataProcessingMonitoringData] = useState<DataProcessingMonitoringResponse>({processingDays: 0, avgTimePerDay: '-', requestTime: '-', processingTime: '-'});
    const [sttProcessingMonitoringData, setSttProcessingMonitoringData] = useState<SttProcessingMonitoringResponse>({sttRequestCount: 0, sttWaitingCount: 0, sttProgressCount: 0, sttCompletedCount: 0, sttErrorCount: 0});
    const [searchParam, setSearchParam] = useState<SearchParam>({startDate: moment(date).subtract(1, 'months').format('yyyy-MM-DD'), endDate: moment(date).format('yyyy-MM-DD')});
    const [sttSearchParam, setSttSearchParam] = useState<SttSearchParam>({date: moment().format('yyyy-MM-DD')});
    const [_, forceUpdate] = useReducer((x) => x + 1, 0);
    const _api = {
        data: deepCopyObject(API.DATA_PROCESSING_MONITORING),
        stt: deepCopyObject(API.STT_PROCESSING_MONITORING)
    };

    const change = (e: ChangeEvent<HTMLSelectElement>) => {
        const time = JSON.parse(e.target.value);
        setSttSearchParam(p => {
            if(time.startTime){
                return Object.assign(p, time);
            }else{
                delete p.startTime;
                delete p.endTime;
                return p;
            }
        });
    };

    const dataDateChange = (key: string, d: Date) => {
        if(key === 'startDate'){
            if(moment.duration(moment(d).diff(moment(searchParam.endDate))).asDays() >= 0){
                setSearchParam(p => (Object.assign(p, {endDate: moment(d).format('YYYY-MM-DD')})))
            }
        }
        setSearchParam(p => (Object.assign(p, {[key]: moment(d).format('YYYY-MM-DD')})))
        forceUpdate();
    };

    const sttDateChange = (d: Date) => {
        const date = {date: moment(d).format('YYYY-MM-DD')};
        setSttSearchParam(p => (Object.assign(p, date)));
        forceUpdate();
    };

    const callDataProcessingMonitoringData = useCallback(async () => {
        try {
            await callApi(_api.data, searchParam).then((res)=>{
                if(res.status === 200){
                    setDataProcessingMonitoringData(res.data);
                }else{
                    notiStore.error(res.data.message);
                    console.log('%c HTTP status : ' + res.status + ' : callDataProcessingMonitoringData fail\n', res.data.message);
                }
            }).catch((res)=>{
                console.log(res);
                notiStore.error(res.data.message);
                console.log('%c HTTP status : ' + res.status + ' : callDataProcessingMonitoringData fail\n', res.data.message);
            });
        } catch {
            console.log('error');
        }
    },[dataProcessingMonitoringData]);

    const callSttProcessingMonitoringData = useCallback(async () => {
        try {
            await callApi(_api.stt, sttSearchParam).then((res)=>{
                if(res.status === 200){
                    setSttProcessingMonitoringData(res.data);
                }else{
                    notiStore.error(res.data.message);
                    console.log('%c HTTP status : ' + res.status + ' : callSttProcessingMonitoringData fail\n', res.data.message);
                }
            }).catch((res)=>{
                console.log(res);
                notiStore.error(res.data.message);
                console.log('%c HTTP status : ' + res.status + ' : callSttProcessingMonitoringData fail\n', res.data.message);
            });
        } catch {
            console.log('error');
        }
    },[sttProcessingMonitoringData]);

    useEffect(() => {
        callDataProcessingMonitoringData().then(r => {
            console.log('call DataProcessingMonitoringData');
        });
        callSttProcessingMonitoringData().then(r => {
            console.log('call SttProcessingMonitoringData');
        });
    }, []);

    return (
        <>
            <br/>
            <br/>
            <ComPage title={'데이터 처리 모니터링 현황'} subTitle={''} isVisibleSpan={false} engineName={''}>
                <div className="srchArea inline">
                    <table className="tbl_line_view" summary="조회 일자로 구성">
                        <caption className="hide">검색조건</caption>
                        <colgroup>
                            <col width={100} /><col width={350} />
                        </colgroup>
                        <tbody>
                        <tr>
                            <th scope="row">조회 일자</th>
                            <td>
                                <div className="iptBox">
                                    <DatePicker className="iptDateTime" dateFormat="yyyy-MM-dd" locale={ko} onChange={date => dataDateChange('startDate', date)} selectsStart startDate={new Date(searchParam.startDate)} endDate={new Date(searchParam.endDate)} selected={new Date(searchParam.startDate)} maxDate={new Date()}/>
                                    <span>‐</span>
                                    <DatePicker className="iptDateTime" dateFormat="yyyy-MM-dd" locale={ko} onChange={date => dataDateChange('endDate', date)} selectsEnd minDate={new Date(searchParam.startDate)} startDate={new Date(searchParam.startDate)} endDate={new Date(searchParam.endDate)} selected={new Date(searchParam.endDate)} maxDate={new Date()}/>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div className="btn_box">
                        <a onClick={callDataProcessingMonitoringData} className="btn_srch">조회</a>
                    </div>
                </div>
                <div className="tbl_lstBox">
                    <table summary="처리 일수, 일 평균시간, 요청시간, 처리시간으로 구성" className="tbl_lst center">
                        <colgroup>
                            <col width={100} />
                            <col width={100} />
                            <col width={100} />
                            <col width={100} />
                        </colgroup>
                        <thead>
                        <tr>
                            <th>조회 일수</th>
                            <th>일 평균 녹취시간</th>
                            <th>총 녹취시간</th>
                            <th>STT 완료 녹취시간</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><span>{dataProcessingMonitoringData.processingDays}</span>일</td>
                            <td>{dataProcessingMonitoringData.avgTimePerDay}</td>
                            <td>{dataProcessingMonitoringData.requestTime}</td>
                            <td>{dataProcessingMonitoringData.processingTime}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div className="srchArea inline">
                    <table className="tbl_line_view" summary="STT 처리 일자, STT 처리 시간으로 구성">
                        <caption className="hide">검색조건</caption>
                        <colgroup>
                            <col width={100} /><col width={200} /><col width={100} /><col width={200} />
                        </colgroup>
                        <tbody>
                        <tr>
                            <th scope="row">STT 요청 일자</th>
                            <td>
                                <div className="iptBox">
                                    <DatePicker className="iptDateTime" dateFormat="yyyy-MM-dd" locale={ko} onChange={sttDateChange} selected={new Date(sttSearchParam.date)}/>
                                </div>
                            </td>
                            <th scope="row">STT 요청 시간</th>
                            <td>
                                <div className="iptBox">
                                    <select onChange={ e => change(e) } className="select">
                                        <option value={JSON.stringify({startTime: null, endTime: null})}>전체</option>
                                        {
                                            [...Array(11).keys()].map(i => {
                                                const v = i+8;
                                                return <option value={JSON.stringify({startTime: (v > 9 ? v : '0'+v)+':00', endTime: (v+1 > 9 ? v+1 : '0'+(v+1))+':00'})}>{v > 9 ? v : '0'+v}:00~{v+1 > 9 ? v+1 : '0'+(v+1)}:00</option>
                                            })
                                        }
                                    </select>
                                </div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <div className="btn_box">
                        <a onClick={callSttProcessingMonitoringData} className="btn_srch">조회</a>
                    </div>
                </div>
                <div className="tbl_lstBox">
                    <table summary="STT요청, STT대기, STT처리중, STT오류, STT완료로 구성" className="tbl_lst center">
                        <caption className="hide" />
                        <colgroup>
                            <col width={100} />
                            <col width={100} />
                            <col width={100} />
                            <col width={100} />
                            <col width={100} />
                        </colgroup>
                        <thead>
                        <tr>
                            <th>STT요청</th>
                            <th>STT대기</th>
                            <th>STT처리중</th>
                            <th>STT오류</th>
                            <th>STT완료</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{sttProcessingMonitoringData.sttRequestCount}</td>
                            <td>{sttProcessingMonitoringData.sttWaitingCount}</td>
                            <td>{sttProcessingMonitoringData.sttProgressCount}</td>
                            <td>{sttProcessingMonitoringData.sttErrorCount}</td>
                            <td><span className="bg_rouBox complete">{sttProcessingMonitoringData.sttCompletedCount}</span></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </ComPage>
        </>
    );
}

export default ConDataProcessingMonitoring;
