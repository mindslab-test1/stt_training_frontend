import React, {ChangeEvent, useCallback, useContext, useEffect, useState} from 'react';
import {ComPage} from '../../components/commons';
import {API, callApi} from "../../service/ApiService";
import {deepCopyObject} from "../../utils/CommonUtils";
import RootStore from "../../store/RootStore";

interface Props{}

interface ServiceMonitoringDto {
    servId: string;
    svcId: string;
    svcStatCd: string;
    oprtDate: string;
    svcStDate: string;
    svcEdDate: string;
    tooltip: boolean;
}

interface ServiceMonitoringResponse {
    svcIds: Array<Map<String, String>>;
    servMap: Map<string, Map<string, ServiceMonitoringDto>>;
}

const ConServiceMonitoring: React.FC<Props> = () => {

    const {notiStore} = useContext(RootStore);
    const [serviceMonitoringData, setServiceMonitoringData] = useState<ServiceMonitoringResponse>();
    const [sec, setSec] = useState<number>(0);

    const _api = deepCopyObject(API.SERVICE_MONITORING);
    let timer;

    const callServiceMonitoringData = useCallback(async () => {
        try {
            await callApi(_api).then((res)=>{
                if(res.status === 200){
                    res.data.servMap = Object.keys(res.data.servMap).sort().reduce(
                        (obj, key) => {
                            obj[key] = res.data.servMap[key];
                            return obj;
                        },
                        {}
                    );
                    setServiceMonitoringData(res.data);
                }else{
                    notiStore.error(res.data.message);
                    console.log('%c HTTP status : ' + res.status + ' : callServiceMonitoringData fail\n', res.data.message);
                }
            }).catch((res)=>{
                console.log(res);
                notiStore.error(res.data.message);
                console.log('%c HTTP status : ' + res.status + ' : callServiceMonitoringData fail\n', res.data.message);
            });
        } catch {
            console.log('error');
        }
    },[serviceMonitoringData]);

    const apiCallInterval = async () => {
        if(timer) clearInterval(timer);
        await callServiceMonitoringData();
        if(sec > 0){
            timer = setInterval(() => callServiceMonitoringData(),sec*1000)
        }
    };

    const change = (e: ChangeEvent<HTMLSelectElement>) => {
        setSec(Number(e.target.value));
    };

    const showServiceDetail = (className: string, hover: boolean) => {
        let elem = document.getElementsByClassName(className)[0];
        if(elem instanceof HTMLElement) elem.style.display = hover ? 'block' : 'none';
    };

    useEffect(() => {
        callServiceMonitoringData().then(r => {
            console.log('call ServiceMonitoringData');
        });
    }, []);

    return (
        <ComPage title={'서비스 모니터링 현황'} subTitle={''} isVisibleSpan={false} engineName={''}>
            <div className="srchArea inline">
                <table className="tbl_line_view" summary="조회 주기로 구성">
                    <caption className="hide">검색조건</caption>
                    <colgroup>
                        <col width={100} /><col width={200} />
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="row">조회 주기</th>
                        <td>
                            <select onChange={ e => change(e) } className="select">
                                <option value={0}>1번 조회</option>
                                <option value={5}>5초 마다</option>
                                <option value={30}>30초 마다</option>
                                <option value={60}>60초 마다</option>
                            </select>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div className="btn_box">
                    <a onClick={apiCallInterval} className="btn_srch">조회</a>
                </div>
            </div>
            <div className="tbl_lstBox monitoring">
                <table summary="서비스 목록, 엔진, 서버로 구성" className="tbl_lst center">
                    <caption className="hide">서버처리현황</caption>
                    <thead>
                    <tr>
                        <th scope="col" style={{width: '150px'}}>서비스 목록</th>
                        {serviceMonitoringData?.svcIds.map((v) => <th scope="col"><pre>{v['servId']}{v['servIdNm'] ? '\n('+v['servIdNm']+')' : ''}</pre></th>)}
                    </tr>
                    </thead>
                    <tbody>
                    {
                        Object.keys(serviceMonitoringData?.servMap || {}).map((v, idx) => {
                            return <tr>
                                <th scope="row">
                                    <pre>
                                    {
                                        Object.keys(serviceMonitoringData?.servMap[v] || {}).map((sv, sidx) => {
                                            if(sidx === 0){
                                                const serv = serviceMonitoringData?.servMap[v][sv];
                                                return serv.svcId+'\n'+(serv.svcIdNm ? ' ('+serv.svcIdNm+')' : '');
                                            }
                                        })
                                    }
                                    </pre>
                                </th>
                                {
                                    serviceMonitoringData?.svcIds.map((sv, sidx) => {
                                        const key = sv['servId'];
                                        return serviceMonitoringData?.servMap[v][key] ?
                                            serviceMonitoringData?.servMap[v][key].svcStatCd === 'UNIDENTIFIED' ?
                                                <td>
                                                    <div onMouseOver={() => {showServiceDetail('detail'+idx+'-'+sidx, true)}} onMouseOut={() => {showServiceDetail('detail'+idx+'-'+sidx, false)}} className="monitoring_status">
                                                        <a className="unidentified">확인요망</a>
                                                        <ul className={'service_detail detail'+idx+'-'+sidx}>
                                                            <li>1분 후에 다시 시도해주세요.</li>
                                                        </ul>
                                                    </div>
                                                </td>
                                                :
                                                <td>
                                                    <div onMouseOver={() => {showServiceDetail('detail'+idx+'-'+sidx, true)}} onMouseOut={() => {showServiceDetail('detail'+idx+'-'+sidx, false)}} className="monitoring_status">
                                                        <a className={serviceMonitoringData?.servMap[v][key].svcStatCd.toLocaleLowerCase()}>{serviceMonitoringData?.servMap[v][key].svcStatCd}</a>
                                                        <ul className={'service_detail detail'+idx+'-'+sidx}>
                                                            <li>가동시간:<span>{serviceMonitoringData?.servMap[v][key].oprtDate || '-'}</span></li>
                                                            <li>시작시간:<span>{serviceMonitoringData?.servMap[v][key].svcStDate || '-'}</span></li>
                                                            <li>종료시간:<span>{serviceMonitoringData?.servMap[v][key].svcEdDate || '-'}</span></li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            :
                                            <td>
                                                <div className="monitoring_status">
                                                    <a className="no_value">해당없음</a>
                                                </div>
                                            </td>
                                    })
                                }
                            </tr>
                        })
                    }
                    </tbody>
                </table>
            </div>
        </ComPage>
    );
}

export default ConServiceMonitoring;
