import React, {useCallback, useContext, useState} from 'react';
import {ComPage} from 'components/commons';
import ComEngineCard from '../../components/commons/ComEngineCard';
import ComPopBox from '../../components/commons/ComPopBox';
import PopCreateProject from '../../components/popup/PopCreateProject';
import {IContainerBaseProps} from '../../constants/BaseInterface';
import imgAutoMLStt from 'assets/images/img_autoML_stt.png';
import RootStore from '../../store/RootStore';
import {isLoggedIn} from '../../service/AuthService';
import {ERouteUrl} from '../../router/RouteLinks';

interface Props extends IContainerBaseProps {}

const ConProjectCreate: React.FC<Props> = (navigation) => {
    const [selectedEngineName, setSelectedEngineName] = useState('')
    const [selectedEngineUrl, setSelectedEngineUrl] = useState('')
    const [popShow, setPopShow] = useState(false);
    const {layoutStore} = useContext(RootStore);
    const { toggleHeader, toggleSidebar } = layoutStore;

    const CardList = [
        {
            id: 0,
            engineName:'STT',
            engineLowerCase:'stt',
            imgPath: imgAutoMLStt,
            title:'음성을 문자로 바꾸는 AI',
            text:'가장 뛰어난 음성인식 모델 학습 시작하기',
        },
        {
            id: 1,
            engineName:'LM',
            engineLowerCase:'lm',
            imgPath: imgAutoMLStt,
            title:'LM',
            text:'LM 학습 시작하기',
        }
    ]

    const setPopIsOpen = (engineName, engineUrl) => {
      // Auth Ping Test;
      isLoggedIn()
        .then((res) => {
          if (!res) {
            navigation.history.replace(ERouteUrl.SSO_LOGIN);
          }
        })
        .catch(() => {
          navigation.history.replace(ERouteUrl.SSO_LOGIN);
        });

      setPopShow(!popShow);
      setSelectedEngineName( engineName );
      setSelectedEngineUrl(engineUrl);
    }

    const closePopup = () => {
        setPopShow(false);
    }

  return (
    <ComPage title={'프로젝트 시작하기'} subTitle={'다양한 엔진을 간편하게 고품질 커스텀 머신러닝 모델을 학습시키고 나만의 멋진 AI를 만들어 보세요.'} isVisibleSpan={false} engineName={''}>
        <ul className='lst_engine'>
          {CardList.map((engineCard)=>(
              <ComEngineCard
                  key={engineCard.id}
                  setPopIsOpen={setPopIsOpen}
                  engineName={engineCard.engineName}
                  engineLowerCase ={engineCard.engineLowerCase}
                  imgPath={engineCard.imgPath}
                  title={engineCard.title}
                  text={engineCard.text}
              />
          ))}
        </ul>
        <ComPopBox show={popShow}>
            <PopCreateProject navigation={navigation} close={closePopup} engineUrl={selectedEngineUrl} engineName={selectedEngineName}/>
        </ComPopBox>
    </ComPage>
  );

};

export default ConProjectCreate;
