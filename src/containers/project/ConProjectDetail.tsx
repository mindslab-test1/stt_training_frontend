import React, {useContext, useEffect, useState} from 'react';
import {API, callApi, IApiDefinition} from '../../service/ApiService';
import {deepCopyObject} from '../../utils/CommonUtils';
import RootStore from '../../store/RootStore';

interface Params {
  id: number | string;
  refreshCount?: number;
  children: any;
}

const useGetResultApi = (api: IApiDefinition, urlKey: string, urlId: any, _refreshCount: number | undefined) => {
  const [refreshCount, setRefreshCount] = useState(1);
  if (_refreshCount && _refreshCount !== refreshCount) {
    setRefreshCount(_refreshCount);
  }
  const [type, setType] = useState(null);
  const {sttProjectStore, lmProjectStore, legacyDataStore} = useContext(RootStore);

  useEffect(() => {
    if (urlKey && urlId) {
      api = deepCopyObject(api);
      api.url = api.url.replace(urlKey, urlId);
    }

    async function fetchData() {
      const res = await callApi(api);
      if (res.success) {
        console.log('get project data success:');
        console.log(res.data);

        if(res.data.projectType === 'STT') {
          await legacyDataStore.fetchLangData();
          await legacyDataStore.fetchSampleRateData();
          await sttProjectStore.fetchData(res.data.id);
        } else if(res.data.projectType === 'LM') {
          await legacyDataStore.fetchLangData();
          await lmProjectStore.fetchData(res.data.id);
        }
        else {
          // tts, etc ...
        }
        setType(res.data.projectType);
      }
    }

    void fetchData();
  }, [refreshCount]);
  return type;
};

export const ConProjectDetail = ({id, refreshCount, children}: Params) => {
  if(!id || !children) return null;
  const type = useGetResultApi(API.PROJECT_INFO, '_ID_', id, refreshCount);
  return children(type);
};
