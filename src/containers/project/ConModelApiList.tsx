import React, {useState} from 'react';
import {ComPage} from '../../components/commons';
import ComPopBox from '../../components/commons/ComPopBox';
import PopModelApi from '../../components/popup/PopModeApi';

interface Props{}

const ConModelApiList: React.FC<Props> = () => {

    const [popShow, setPopShow] = useState(false);

    const setPopIsOpen = () => {
        console.log('ModelAPI == setPopIsOpen');
        setPopShow(!popShow);
    }

    const closePopup = () => {
        console.log(popShow);
        setPopShow(false);
    }

    return (
        <ComPage title={'내가 만든 모델 API'} subTitle={''} isVisibleSpan={false} engineName={''}>
            <div className='tbl_lstBox'>
                <table className='tbl_lst'>
                    <caption className='hide'>내가 만든 모델 API 목록</caption>
                    <colgroup>
                        <col /><col /><col width='130' /><col width='180' /><col width='130' />
                    </colgroup>
                    <thead>
                    <tr>
                        <th scope='col'>모델 API 명</th>
                        <th scope='col'>선택된 학습모델</th>
                        <th scope='col'>모델 유형</th>
                        <th scope='col'>API 생성일 시</th>
                        <th scope='col'>정보 보기</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td scope='row' className='modelApiName'>oooooooo전시용 STT API</td>
                        <td>Item 1.1</td>
                        <td><span className='bg_rouBox stt'>STT</span></td>
                        <td>2020-10-21 18:00:00</td>
                        <td><a className='btn_line btn_lyr_open' href='#lyr_apiInfo' onClick={setPopIsOpen}>보기</a></td>
                    </tr>
                    <tr>
                        <td scope='row' className='modelApiName'>vbxc asdf werbsa 과제용 STT</td>
                        <td>Item 2.1</td>
                        <td><span className='bg_rouBox stt'>STT</span></td>
                        <td>2020-10-21 18:00:00</td>
                        <td><a className='btn_line btn_lyr_open' href='#lyr_apiInfo' onClick={setPopIsOpen}>보기</a></td>
                    </tr>
                    <tr>
                        <td scope='row' className='modelApiName'>일이삼사오육칠팔구십일이삼사오육칠팔구십일이삼사오육칠팔구십</td>
                        <td>Item 2.2</td>
                        <td><span className='bg_rouBox stt'>STT</span></td>
                        <td>2020-10-21 18:00:00</td>
                        <td><a className='btn_line btn_lyr_open' href='#lyr_apiInfo' onClick={setPopIsOpen}>보기</a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            {/*<Paging />*/}

            <ComPopBox show={popShow}>
                <PopModelApi close={closePopup}/>
            </ComPopBox>
        </ComPage>
    );
}

export default ConModelApiList;
